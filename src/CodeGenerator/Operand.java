package CodeGenerator;

import CodeGenerator.Descriptors.PrimitiveVariableType;


/**
 * Created by rasa on 2/1/17.
 */
public class Operand {


    private AddressingMode addressingMode;
    private PrimitiveVariableType primitiveVariableType;
    private Object value;
    private boolean isEmptyStructAssignment;
    private boolean isStringPointer;


    /**
     * @param primitiveVariableType
     * @param addressingMode
     * @param value                 pay attention that this value must be integer address for
     *                              all modes except immediate. for immediate it must
     *                              be of type specified by variableType
     *                              THE TYPE IS NOT CHECKED USE WITH CAUTION
     * @return operand as expected by VM
     */
    public Operand(AddressingMode addressingMode, PrimitiveVariableType primitiveVariableType, Object value) {
        this.addressingMode = addressingMode;
        this.primitiveVariableType = primitiveVariableType;
        this.value = value;
        this.isStringPointer = false;
        this.isEmptyStructAssignment = false;
    }

    public AddressingMode getAddressingMode() {
        return addressingMode;
    }

    public PrimitiveVariableType getPrimitiveVariableType() {
        return primitiveVariableType;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public boolean isStringPointer() {
        return isStringPointer;
    }

    public void setStringPointer(boolean strinPointer) {
        isStringPointer = strinPointer;
    }

    public void makeGlobal() {
        if (addressingMode.equals(AddressingMode.LocalIndirect))
            addressingMode = AddressingMode.GlobalIndirect;
        if (addressingMode.equals(AddressingMode.LocalDirect))
            addressingMode = AddressingMode.GlobalDirect;
    }


    @Override
    public String toString() {
        return this.addressingMode.getPrefix() + this.primitiveVariableType.getPrefix() + this.value.toString();
    }

    public boolean isEmptyStructAssignment() {
        return isEmptyStructAssignment;
    }

    public void setEmptyStructAssignment(boolean emptyStructAssignment) {
        isEmptyStructAssignment = emptyStructAssignment;
    }
}
