package CodeGenerator;

import CodeGenerator.Descriptors.*;
import Exceptions.*;
import Scanner.Scanner;
import Scanner.ScannerTypes;
import Scanner.Symbols;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Stack;

import static CodeGenerator.AddressingMode.*;
import static CodeGenerator.Descriptors.PrimitiveVariableType.*;
import static CodeGenerator.Descriptors.VariableType.*;
import static CodeGenerator.Descriptors.VariableType.String;

public class CodeGenerator implements ScannerTypes, Symbols {
    public static final HashMap<String, PrimitiveVariableType> scannerType_CgType = new HashMap<>();
    public static final Operand immideiateZero = getImmediateOperand(0);
    public static final Operand immideiateOne = getImmediateOperand(1);
    public static final Operand immideiateMinusOne = getImmediateOperand(-1);
    public static final Operand immideiateNullCharAsInt = getImmediateOperand(getAscii('\0'));
    public static final Operand immideiateEndLineCharAsInt = getImmediateOperand(getAscii('\n'));
    public static final Operand codeEndLineImmideiate = getImmediateOperand(-1);
    public static final int maxStringInputSize = 10000;
    public static final Delimiter delimiter = new Delimiter();
    public static final int fullOneInt = -1;
    public static final SemanticStackBottom semanticStackBottom = new SemanticStackBottom();
    public static final EmptyInStructAssign emptyStructAssign = new EmptyInStructAssign();
    static {
        scannerType_CgType.put(INT, Int);
        scannerType_CgType.put(LONG, PrimitiveVariableType.LongInt);
        scannerType_CgType.put(CHAR, PrimitiveVariableType.Char);
        scannerType_CgType.put(BOOL, PrimitiveVariableType.Bool);
        scannerType_CgType.put(REAL, PrimitiveVariableType.Real);
    }

    private Scanner scanner; // This was my way of informing CG about Constant Values detected by Scanner, you can do whatever you like
    private ActivityRecord activityRecord;
    // Define any variables needed for code generation
    private Integer tempCount;
    private ArrayList<VMCodeLine> code;
    private CodeLocation location;
    private Stack<CodeLocation> locationStack;
    private Stack<Object> semanticStack;
    private Stack<Operand> breakStack;
    private Stack<Operand> continueStack;
    private Descriptor lastDefinedIDDesc;
    private String lastDefinedIDSym;
    private FunctionDescriptor lastFunction;
    private Stack<Descriptor> locationDesc;
    private boolean inMain;
    private boolean problematicReturn;
    private boolean noReturn;
    private boolean inFuncDef;
    /**
     * need to run Parser only just for syntax
     * and another time for code generation
     * just read the code twice
     * first time this variable is false
     * then toggle it
     */
    private boolean codeGeneratingPhase;

    public CodeGenerator(Scanner scanner) {
        this.scanner = scanner;
        this.code = new ArrayList<>();
        this.tempCount = 0;
        this.codeGeneratingPhase = false;
        this.activityRecord = new ActivityRecord();
        this.location = CodeLocation.Global;
        this.semanticStack = new Stack<>();
        this.locationStack = new Stack<>();
        this.breakStack = new Stack<>();
        this.continueStack = new Stack<>();
        this.inMain = false;
        this.problematicReturn = false;
        this.noReturn = false;
        this.lastFunction = null;
        this.semanticStack.push(semanticStackBottom);
        this.locationDesc = new Stack<>();
    }

    /**
     * @param l A long number
     * @return An ArrayList of two integer
     * the first int is significant 4 bytes of long
     * and the second is less significant 4 bytes
     */
    public static ArrayList<Integer> longAsInts(Long l) {
        int mostSignificantWord = Math.toIntExact((l >>> 32));
        int leastSignificantWord = Math.toIntExact((l << 32) >>> 32);
        ArrayList<Integer> res = new ArrayList<>();
        res.add(mostSignificantWord);
        res.add(leastSignificantWord);
        return res;
    }

    public static int getAscii(char ch) {
        return (int) ch;
    }

    private static Operand getImmediateOperand(Object value) {
        if (value instanceof Integer) {
            return new Operand(AddressingMode.Immidiate, Int, value);
        } else if (value instanceof Character) {
            return new Operand(AddressingMode.Immidiate, PrimitiveVariableType.Char, value);
        } else if (value instanceof Boolean) {
            return new Operand(AddressingMode.Immidiate, PrimitiveVariableType.Bool, value);
        } else if (value instanceof Float) {
            return new Operand(AddressingMode.Immidiate, PrimitiveVariableType.Real, value);
        }
        return null;
    }

    public void Generate(String semanticAction) throws CodeGeneratorException {
        if (!this.codeGeneratingPhase) {
            return;
        }

        if (semanticAction.equals("NoSem")) {
            return;
        }
        System.err.println("Running: " + semanticAction); // Just for debug

        switch (semanticAction) {
            case "NoSem":
                break;
            case "@setLocation":
                setLocation();
                break;
            case "@popLocation":
                popLocation();
                break;
            case "@popLocationRecord":
                popLocationRecord();
                break;
            case "@addRecord":
                addRecord();
                break;
            case "@popRecord":
                popRecord();
                break;
            case "@pushTokenValue":
                pushTokenValue();
                break;
            case "@pushMatchedText":
                pushMatchedText();
                break;
            case "@pop":
                pop();
                break;
            case "@defineNonArrayVariable":
                defineNonArrayVariable();
                break;
            case "@defineArrayVariable":
                defineArrayVariable();
                break;
            case "@pushLastDefinedIdDesc":
                pushLastDefinedIdDesc();
                break;
            case "@initStructAndPush":
                initStructAndPush();
                break;
            case "@pushEmpInStruct":
                pushEmpInStruct();
                break;
            case "@pES_sDA":
                pES_sDA();
                break;
            case "@compStructDef":
                compStructDef();
                break;
            case "@initEnvAndPush":
                initEnvAndPush();
                break;
            case "@compEnvDef":
                compEnvDef();
                break;
            case "@goto":
                gotoCg();
                break;
            case "@label":
                label();
                break;
            case "@makeDescAndSetLastFunction":
                makeDescAndSetLastFunction();
                break;
            case "@addOutputToFunc":
                addOutputToFunc();
                break;
            case "@pushUnderDesc":
                pushUnderDesc();
                break;
            case "@addInputToFunc":
                addInputToFunc();
                break;
            case "@jmp_StAdr_inass_addGlob":
                jmp_StAdr_inass_addGlob();
                break;
            case "@cmpJ_DataSize_unsetLastFunc":
                cmpJ_DataSize_unsetLastFunc();
                break;
            case "@inMain_ChkOutput":
                inMain_ChkOutput();
                break;
            case "@OutMain_Pop_unsetLastFunc":
                OutMain_Pop_unsetLastFunc();
                break;
            case "@logicalOp_Push":
                logicalOp_Push();
                break;
            case "@binOp_push":
                binOp_push();
                break;
            case "@relationOp_push":
                relationOp_push();
                break;
            case "@arithOp_push":
                arithOp_push();
                break;
            case "@unaryOp_push":
                unaryOp_push();
                break;
            case "@makeConst_Push":
                makeConst_Push();
                break;
            case "@strlen_push":
                strlen_push();
                break;
            case "@concat_push":
                concat_push();
                break;
            case "@computeInEnvOperand_push":
                computeInEnvOperand_push();
                break;
            case "@computeInStructOperand_Push":
                computeInStructOperand_Push();
                break;
            case "@computeOperand_Push":
                computeOperand_Push();
                break;
            case "@pushDelimiter":
                pushDelimiter();
                break;
            case "@computeElementAdr_pushOp":
                computeElementAdr_pushOp();
                break;
            case "@computeOperandOrFunc_Push":
                computeOperandOrFunc_Push();
                break;
            case "@isVoid_Push":
                isVoid_Push();
                break;
            case "@pushLastDefinedIdOp":
                pushLastDefinedIdOp();
                break;
            case "@arrayDefAssignment":
                arrayDefAssignment();
                break;
            case "@structDefAssignment":
                structDefAssignment();
                break;
            case "@normalAssignment":
                normalAssignment();
                break;
            case "@callFunctionWithOut":
                callFunction(true);
                break;
            case "@callFunctionWithoutOut":
                callFunction(false);
                break;
            case "@initReturn":
                initReturn();
                break;
            case "@compReturn":
                compReturn();
                break;
            case "@checkRet":
                checkRet();
                break;
            case "@release":
                release();
                break;
            case "@write":
                write();
                break;
            case "@makeJ_Push":
                makeJ_Push();
                break;
            case "@popRecord_NOP_compJ":
                popRecord_NOP_compJ();
                break;
            case "@makeJ_push_addRecord":
                makeJ_push_addRecord();
                break;
            case "@compJ_popRecord":
                compJ_popRecord();
                break;
            case "@makeJ":
                makeJ();
                break;
            case "@initContBreak_push":
                initContBreak_push();
                break;
            case "@popRecord_compContBreakPop":
                popRecord_compContBreakPop();
                break;
            case "@jumpBreak":
                jumpBreak();
                break;
            case "@jumpContinue":
                jumpContinue();
                break;
            case "@addRecord_initContBrBJ_push":
                addRecord_initContBrBJ_push();
                break;
            case "@compBr_popBrCont":
                compBr_popBrCont();
                break;
            case "@makeDoWhileFinalJump":
                makeDoWhileFinalJump();
                break;
            case "@compCont":
                compCont();
                break;
            case "@read":
                read();
                break;
            case "@initEndAdr_push":
                initEndAdr_push();
                break;
            case "@pushBlockAdrOp_pushConsUnderEndAdr":
                pushBlockAdrOp_pushConsUnderEndAdr();
                break;
            case "@popRecord_jumpToEnd":
                popRecord_jumpToEnd();
                break;
            case "@compSwitchCase":
                compSwitchCase();
                break;
            default:
                System.err.println("The Action " + semanticAction + " is not defined");
        }
    }

    public void FinishCode() // You may need this
    {
        if (!codeGeneratingPhase) {
            return;
        }
        codeEndLineImmideiate.setValue(getNextLineAddress());
        code.add(VMCodeLine.NOP());
        if (semanticStack.size() != 1 || !semanticStack.peek().equals(semanticStackBottom)) {
            System.err.println("Non Empty Semantic Stack at end!");
        }
        semanticStack.pop();
    }

    public void WriteOutput(String outputName) throws IOException {
        // Can be used to print the generated code to output
        // I used this because in the process of compiling, I stored the generated code in a structure
        // If you want, you can output a code line just when it is generated (strongly NOT recommended!!)
        FileWriter fileWriter = new FileWriter(outputName);
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < code.size(); ++i) {
            sb.append(code.get(i).toString());
            sb.append("\n");
        }
        fileWriter.write(sb.toString());
        fileWriter.close();
    }

    /**
     * @return a new unique name for new temp variable
     * to add to symbol table
     */
    private String getNextTempSymbol() {
        String name = "_temp_" + tempCount.toString();
        tempCount += 1;
        return name;
    }

    public int getNextLineAddress() {
        return code.size();
    }

    public void setCodeGeneration(boolean generate) {
        this.codeGeneratingPhase = generate;
    }

    private void setLocation() throws DuplicateSymbolAddException, GlobalRecordRemoveException, NonExistentLabelException {
        int token = scanner.currentToken();
        switch (token) {
            case ENVIRONMENT:
                locationStack.push(location);
                location = CodeLocation.Environment;
                break;
            case FUNCTION:
                locationStack.push(location);
                location = CodeLocation.Function;
                this.activityRecord.addNewRecord();
                break;
            case STRUCTURE:
                locationStack.push(location);
                location = CodeLocation.Structure;
                break;
        }
    }

    private void popLocation() {
        location = locationStack.pop();
    }

    private void popLocationRecord() throws DuplicateSymbolAddException, GlobalRecordRemoveException, NonExistentLabelException {
        location = locationStack.pop();
        activityRecord.popRecord();
    }

    private void pushTokenValue() {
        semanticStack.push(scanner.tokenValue());
    }

    private void pushMatchedText() {
        semanticStack.push(scanner.matchedText());
    }

    private void pop() {
        semanticStack.pop();
    }

    private void addRecord() {
        activityRecord.addNewRecord();
    }

    private void popRecord() throws DuplicateSymbolAddException, GlobalRecordRemoveException, NonExistentLabelException {
        activityRecord.popRecord();
    }

    private void defineNonArrayVariable() throws DuplicateSymbolAddException, NonExistentSymbolException, notVariableTypeException, DuplicateVariableException, IllegalTypeInStructException, IllegalTypeInEnvException, ArrayInsideStructException {
        String symbol = scanner.matchedText();
        String type = (String) semanticStack.pop();
        VariableDescriptor desc;
        int address = getFirstEmptyAdrInScope();
        if (scannerType_CgType.get(type) != null) {
            PrimitiveVariableType pvt = scannerType_CgType.get(type);
            desc = new PrimitiveVariableDescriptor(address, pvt);
        } else if (type == STRINGTYPE) {
            desc = new StringVariableDescriptor(address);
            if (!this.inFuncDef && !location.equals(CodeLocation.Structure)) {
                //assign -1 to value meaning null
                code.add(VMCodeLine.assign(immideiateMinusOne, new Operand(getAddressingModeInCode(), Int, address)));
            }
        } else {
            Descriptor structDesc = activityRecord.getSymbolDecriptor(type);
            if (structDesc.getType() != Type.Structure) {
                throw new notVariableTypeException();
            }
            desc = new StructureVariableDescriptor(address, (StructureDescriptor) structDesc);
            if (!this.inFuncDef && !location.equals(CodeLocation.Structure)) {
                //assign -1 to value meaning null
                code.add(VMCodeLine.assign(immideiateMinusOne, new Operand(getAddressingModeInCode(), Int, address)));
            }
        }
        AddSymbolToCurrentScope(symbol, desc);
        lastDefinedIDDesc = desc;
        lastDefinedIDSym = symbol;
    }

    private void defineArrayVariable() throws NonExistentSymbolException, notVariableTypeException, DuplicateSymbolAddException, IllegalTypeInStructException, DuplicateVariableException, IllegalTypeInEnvException, ArrayInsideStructException {
        if (this.location.equals(CodeLocation.Structure)) {
            throw new IllegalTypeInEnvException();
        }
        String symbol = scanner.matchedText();
        String type = (String) semanticStack.pop();
        int address = getFirstEmptyAdrInScope();

        ArrayVariableDescriptor desc;
        if (scannerType_CgType.get(type) != null) {
            PrimitiveVariableType pvt = scannerType_CgType.get(type);
            desc = new ArrayOfPrimitiveDescriptor(address, pvt);
        } else if (type == STRINGTYPE) {
            desc = new ArrayOfStringDescriptor(address);
        } else {
            Descriptor structDesc = activityRecord.getSymbolDecriptor(type);
            if (structDesc.getType() != Type.Structure) {
                throw new notVariableTypeException();
            }
            desc = new ArrayOfStructureVariableDescriptor(address, (StructureDescriptor) structDesc);
        }
        AddSymbolToCurrentScope(symbol, desc);
        lastDefinedIDDesc = desc;
        lastDefinedIDSym = symbol;
        if (!this.inFuncDef && !location.equals(CodeLocation.Structure)) {
            //assign -1 to value meaning null
            code.add(VMCodeLine.assign(immideiateMinusOne, new Operand(getAddressingModeInCode(), Int, address)));
        }
    }

    private void AddSymbolToCurrentScope(String symbol, Descriptor desc) throws DuplicateVariableException, IllegalTypeInStructException, DuplicateSymbolAddException, IllegalTypeInEnvException, ArrayInsideStructException {

        if (location == CodeLocation.Environment) {
            try {
                ((EnvironmentDescriptor) locationDesc.peek()).addSymbol(symbol, (VariableDescriptor) desc);

            } catch (ClassCastException e) {
                throw new IllegalTypeInEnvException();
            }
        } else if (location == CodeLocation.Structure) {
            try {
                if (desc instanceof ArrayVariableDescriptor) {
                    throw new ArrayInsideStructException();
                }
                ((StructureDescriptor) locationDesc.peek()).addSymbol(symbol, (VariableDescriptor) desc);
            } catch (ClassCastException e) {
                throw new IllegalTypeInStructException();
            }
        } else {
            activityRecord.addSymbol(symbol, desc);
        }
    }

    private void pushLastDefinedIdDesc() {
        semanticStack.push(lastDefinedIDDesc);
    }


    private void initStructAndPush() throws DuplicateSymbolAddException {
        String symbol = scanner.matchedText();
        StructureDescriptor desc = new StructureDescriptor();
        semanticStack.push(symbol);
        locationDesc.push(desc);
        activityRecord.addSymbol(symbol, desc);
    }

    private void pushEmpInStruct() {
        semanticStack.push(emptyStructAssign);
    }

    private void pES_sDA() throws CodeGeneratorException {
        pushEmpInStruct();
        structDefAssignment();
    }

    private void compStructDef() throws DuplicateSymbolAddException {
        lastDefinedIDDesc = locationDesc.pop();
        lastDefinedIDSym = (String) semanticStack.pop();
    }

    private void initEnvAndPush() throws DuplicateSymbolAddException {
        String symbol = scanner.matchedText();
        int address = activityRecord.atGlobalScope() ? activityRecord.getGlobalDataAddressCounter() : activityRecord.getLocalDataAddressCounter();
        EnvironmentDescriptor desc = new EnvironmentDescriptor(address);
        semanticStack.push(symbol);
        locationDesc.push(desc);
    }

    private void compEnvDef() throws DuplicateSymbolAddException {
        lastDefinedIDDesc = locationDesc.pop();
        lastDefinedIDSym = (String) semanticStack.pop();
        activityRecord.addSymbol(lastDefinedIDSym, lastDefinedIDDesc);
    }

    private int getFirstEmptyAdrInScope() {
        int address = 0;
        if (location == CodeLocation.Global) {
            address = activityRecord.getGlobalDataAddressCounter();
        } else if (location == CodeLocation.Function) {
            address = activityRecord.getLocalDataAddressCounter();
        } else if (location == CodeLocation.Environment) {
            address = locationStack.peek().equals(CodeLocation.Global) ? activityRecord.getGlobalDataAddressCounter() :
                    activityRecord.getLocalDataAddressCounter();
            address += ((EnvironmentDescriptor) locationDesc.peek()).getDataSize();
        } else if (location == CodeLocation.Structure) {
            address = ((StructureDescriptor) locationDesc.peek()).getDataSize();
        }
        return address;
    }

    private void gotoCg() throws NonExistentSymbolException, GotoNoneLabelException, DuplicateSymbolAddException {
        String label = scanner.matchedText();
        Descriptor prevDef;
        try {
            prevDef = activityRecord.getSymbolDecriptor(label);
        } catch (NonExistentSymbolException e) {
            prevDef = null;
        }
        if (prevDef == null) {
            LabelDescriptor ld = new LabelDescriptor();
            ld.setLineNum(-1);
            activityRecord.addSymbol(label, ld);
            ld.addGoto(this.getNextLineAddress());
            code.add(VMCodeLine.jump(null));

        } else {
            if (!(prevDef instanceof LabelDescriptor)) {
                throw new GotoNoneLabelException();
            }
            LabelDescriptor ld = (LabelDescriptor) prevDef;
            ld.addGoto(this.getNextLineAddress());
            if (ld.getLineNum() != -1) {
                code.add(VMCodeLine.jump(getImmediateOperand(ld.getLineNum())));
            } else {
                code.add(VMCodeLine.jump(null));
                ld.addGoto(getNextLineAddress());
            }
        }

    }

    private void label() throws DuplicateSymbolAddException {

        String label = scanner.matchedText();
        Descriptor prevDef;
        try {
            prevDef = activityRecord.getSymbolDecriptor(label);
        } catch (NonExistentSymbolException e) {
            prevDef = null;
        }
        if (prevDef == null) {
            LabelDescriptor ld = new LabelDescriptor();
            ld.setLineNum(this.getNextLineAddress());
            activityRecord.addSymbol(label, ld);
        } else {
            if (!(prevDef instanceof LabelDescriptor)) {
                throw new DuplicateSymbolAddException();
            }
            LabelDescriptor ld = (LabelDescriptor) prevDef;
            ld.setLineNum(this.getNextLineAddress());
            for (int line : ld.getGotoLines()) {
                VMCodeLine jumpLine = code.get(line);
                VMCodeLine.completeJump(jumpLine, ld.getLineNum());
            }
            ld.getGotoLines().clear();
        }
    }

    private void makeDescAndSetLastFunction() {
        FunctionDescriptor fd = new FunctionDescriptor();
        this.lastFunction = fd;
        this.noReturn = true;
        this.inFuncDef = true;
    }

    private void addOutputToFunc() throws NonExistentSymbolException, notVariableTypeException {
        FunctionDescriptor fd = this.lastFunction;

        String type = (String) scanner.tokenValue();
        VariableDescriptor desc;
        if (scannerType_CgType.get(type) != null) {
            PrimitiveVariableType pvt = scannerType_CgType.get(type);
            desc = new PrimitiveVariableDescriptor(-1, pvt);
        } else if (type == STRINGTYPE) {
            desc = new StringVariableDescriptor(-1);
        } else {
            Descriptor structDesc = activityRecord.getSymbolDecriptor(type);
            if (structDesc.getType() != Type.Structure) {
                throw new notVariableTypeException();
            }
            desc = new StructureVariableDescriptor(-1, (StructureDescriptor) structDesc);
        }

        fd.addOutputSymbol(desc);
    }

    private void pushUnderDesc() {
        semanticStack.push(scanner.matchedText());
    }

    private void addInputToFunc() {
        FunctionDescriptor fd = this.lastFunction;
        fd.addInputSymbol(lastDefinedIDSym, (VariableDescriptor) lastDefinedIDDesc);
    }

    private void jmp_StAdr_inass_addGlob() throws DuplicateSymbolAddException {
        this.inFuncDef = false;
        code.add(VMCodeLine.jump(null));
        FunctionDescriptor fd = lastFunction;
        String functionName = (String) semanticStack.pop();
        fd.setStartAddress(this.getNextLineAddress());
        activityRecord.addSymbolToGlobal(functionName + fd.getInputCount().toString(), fd);
        Operand inputPlaceDir = getTempOperand(Int, LocalDirect);
        Operand inputPlaceInDir = new Operand(LocalIndirect, Int, inputPlaceDir.getValue());
        code.add(VMCodeLine.assign(fd.getInputPlace(), inputPlaceDir));

        for (int i = 0; i < fd.getInputCount(); ++i) {
            VariableDescriptor vd = fd.getInputDescriptors().get(i);
            Operand varInsideFunc;
            if (vd.getVariableType().equals(Primitive)) {
                PrimitiveVariableType pvt = ((PrimitiveVariableDescriptor) vd).getPvt();
                varInsideFunc = new Operand(LocalDirect, pvt, vd.getAddress());
                Operand currentInput = new Operand(LocalIndirect, pvt, inputPlaceDir.getValue());
                code.add(VMCodeLine.assign(currentInput, varInsideFunc));
            } else {
                varInsideFunc = new Operand(LocalDirect, Int, vd.getAddress());
                code.add(VMCodeLine.assign(inputPlaceInDir, varInsideFunc));
            }
            code.add(VMCodeLine.add(immideiateOne, inputPlaceDir, inputPlaceDir));
        }
    }

    private void cmpJ_DataSize_unsetLastFunc() throws NoReturnException {
        if (this.noReturn) {
            throw new NoReturnException();
        }
        FunctionDescriptor fd = this.lastFunction;
        VMCodeLine jumpBeforeFunction = code.get(fd.getStartAddress() - 1);
        VMCodeLine.completeJump(jumpBeforeFunction, getNextLineAddress());
        fd.setDataSize(activityRecord.getLocalDataAddressCounter());
        this.lastFunction = null;
    }

    private void inMain_ChkOutput() throws MainOutputException {
        this.inFuncDef = false;
        this.inMain = true;
        FunctionDescriptor fd = this.lastFunction;
        if (fd.getOutputCount() != 1) {
            throw new MainOutputException();
        }

        VariableDescriptor vd = fd.getOutputDescriptors().get(0);
        if (!vd.getVariableType().equals(Primitive)) {
            throw new MainOutputException();
        }
        PrimitiveVariableDescriptor pvd = (PrimitiveVariableDescriptor) vd;
        if (!pvd.getPvt().equals(Int)) {
            throw new MainOutputException();
        }
    }

    private void OutMain_Pop_unsetLastFunc() throws NoReturnException {
        inMain = false;
        semanticStack.pop();
        this.lastFunction = null;
        if (noReturn) {
            throw new NoReturnException();
        }
        //Todo: maybe free the variables with freeable type
        //OK Maybe NOT
    }

    private void logicalOp_Push() throws CodeGeneratorException {
        Operand op2 = (Operand) semanticStack.pop();
        String operatorSym = (String) semanticStack.pop();
        Operand op1 = (Operand) semanticStack.pop();
        Operand castedOp1 = castCode(op1, PrimitiveVariableType.Bool);
        Operand castedOp2 = castCode(op2, PrimitiveVariableType.Bool);
        AddressingMode mode = getAddressingModeInCode();
        Operand result = getTempOperand(PrimitiveVariableType.Bool, mode);
        switch (operatorSym) {
            case "||":
                code.add(VMCodeLine.logicalOr(castedOp1, castedOp2, result));
                break;
            case "&&":
                code.add(VMCodeLine.logicalAnd(castedOp1, castedOp2, result));
                break;
        }
        semanticStack.push(result);
    }

    private void binOp_push() throws CodeGeneratorException {
        Operand op2 = (Operand) semanticStack.pop();
        String operatorSym = (String) semanticStack.pop();
        Operand op1 = (Operand) semanticStack.pop();
        PrimitiveVariableType resType = PrimitiveVariableType.getCastType(op1.getPrimitiveVariableType(), op2.getPrimitiveVariableType());
        if (!resType.equals(PrimitiveVariableType.LongInt))
            resType = Int;

        Operand castedOp1 = castCode(op1, resType);
        Operand castedOp2 = castCode(op2, resType);
        AddressingMode mode = getAddressingModeInCode();
        Operand result = getTempOperand(resType, mode);

        if (resType.equals(PrimitiveVariableType.LongInt)) {
            Operand resFirstWord = new Operand(mode, Int, result.getValue());
            Operand resSecondWord = new Operand(mode, Int, (int) result.getValue() + 1);
            Operand op1FirstWord = new Operand(mode, Int, castedOp1.getValue());
            Operand op1SecondWord = new Operand(mode, Int, (int) castedOp1.getValue() + 1);
            Operand op2FirstWord = new Operand(mode, Int, castedOp2.getValue());
            Operand op2SecondWord = new Operand(mode, Int, (int) castedOp2.getValue() + 1);

            switch (operatorSym) {
                case "&":
                    code.add(VMCodeLine.binaryAnd(op1FirstWord, op2FirstWord, resFirstWord));
                    code.add(VMCodeLine.binaryAnd(op1SecondWord, op2SecondWord, resSecondWord));
                    break;
                case "|":
                    code.add(VMCodeLine.binaryOr(op1FirstWord, op2FirstWord, resFirstWord));
                    code.add(VMCodeLine.binaryOr(op1SecondWord, op2SecondWord, resSecondWord));
                    break;
                case "^":
                    code.add(VMCodeLine.binaryXor(op1FirstWord, op2FirstWord, resFirstWord));
                    code.add(VMCodeLine.binaryXor(op1SecondWord, op2SecondWord, resSecondWord));
                    break;
            }

        } else {
            switch (operatorSym) {
                case "&":
                    code.add(VMCodeLine.binaryAnd(castedOp1, castedOp2, result));
                    break;
                case "|":
                    code.add(VMCodeLine.binaryOr(castedOp1, castedOp2, result));
                    break;
                case "^":
                    code.add(VMCodeLine.binaryXor(castedOp1, castedOp2, result));

                    break;
            }
        }

        semanticStack.push(result);

    }

    private void shiftLongOp(Operand operand, int count, boolean isLeft) throws CodeGeneratorException {

        AddressingMode mode = operand.getAddressingMode();
        Operand firstWord = new Operand(mode, Int, operand.getValue());
        Operand secondWord = new Operand(mode, Int, (int) operand.getValue() + 1);

        if (!operand.getPrimitiveVariableType().equals(PrimitiveVariableType.LongInt)) {
            throw new CodeGeneratorException("Only for Long");
        }
        if (!operand.getAddressingMode().isDirect()) {
            throw new CodeGeneratorException("Only for Direct");
        }
        if (isLeft) {
            code.add(VMCodeLine.logicalLeftShift(getImmediateOperand(count), firstWord));
            Operand temp = getTempOperand(Int, mode);
            code.add(VMCodeLine.assign(secondWord, temp));
            code.add(VMCodeLine.logicalRightShift(temp, getImmediateOperand(32 - count)));
            code.add(VMCodeLine.add(temp, firstWord, firstWord));
            code.add(VMCodeLine.logicalLeftShift(getImmediateOperand(count), secondWord));
        } else { //meaning the shift is to right
            code.add(VMCodeLine.logicalRightShift(getImmediateOperand(count), secondWord));
            Operand temp = getTempOperand(Int, mode);
            code.add(VMCodeLine.assign(firstWord, temp));
            code.add(VMCodeLine.logicalLeftShift(temp, getImmediateOperand(32 - count)));
            code.add(VMCodeLine.add(temp, secondWord, secondWord));
            code.add(VMCodeLine.logicalRightShift(getImmediateOperand(count), firstWord));
        }
    }

    private void relationOp_push() throws CodeGeneratorException {
        Operand op2 = (Operand) semanticStack.pop();
        String operatorSym = (String) semanticStack.pop();
        Operand op1 = (Operand) semanticStack.pop();
        PrimitiveVariableType castedType = PrimitiveVariableType.getCastType(op1.getPrimitiveVariableType(), op2.getPrimitiveVariableType());
        AddressingMode mode = getAddressingModeInCode();
        Operand result = getTempOperand(PrimitiveVariableType.Bool, mode);
        if (castedType.equals(Int) ||
                castedType.equals(PrimitiveVariableType.Bool) ||
                castedType.equals(PrimitiveVariableType.Char)) {
            castedType = Int;
        } else if (castedType.equals(PrimitiveVariableType.Real)) {
            castedType = PrimitiveVariableType.Real;//Keep it real!
        } else { //LongInt
            castedType = PrimitiveVariableType.LongInt;
        }
        Operand castedOp1 = castCode(op1, castedType);
        Operand castedOp2 = castCode(op2, castedType);

        if (castedType.equals(PrimitiveVariableType.LongInt)) {
            Operand op1FirstWord = new Operand(mode, Int, castedOp1.getValue());
            Operand op1SecondWord = new Operand(mode, Int, (int) castedOp1.getValue() + 1);
            Operand op2FirstWord = new Operand(mode, Int, castedOp2.getValue());
            Operand op2SecondWord = new Operand(mode, Int, (int) castedOp2.getValue() + 1);
            Operand firstWordEqual = getTempOperand(PrimitiveVariableType.Bool, mode);
            Operand secondWordNotEqual = getTempOperand(PrimitiveVariableType.Bool, mode);
            Operand firstWordComp = getTempOperand(PrimitiveVariableType.Bool, mode);
            Operand secondWordComp = getTempOperand(PrimitiveVariableType.Bool, mode);

            code.add(VMCodeLine.equals(op1FirstWord, op2FirstWord, firstWordEqual));
            code.add(VMCodeLine.notEquals(op1SecondWord, op2SecondWord, secondWordNotEqual));
            switch (operatorSym) {
                case "=":
                    code.add(VMCodeLine.equals(op1SecondWord, op2SecondWord, secondWordComp));
                    code.add(VMCodeLine.logicalAnd(secondWordComp, firstWordEqual, result));
                    break;
                case "!=":
                    code.add(VMCodeLine.notEquals(op1FirstWord, op2FirstWord, firstWordComp));
                    code.add(VMCodeLine.notEquals(op1SecondWord, op2SecondWord, secondWordComp));
                    code.add(VMCodeLine.logicalOr(firstWordComp, secondWordComp, result));
                    break;
                case "<":
                    semanticStack.push(op1);
                    semanticStack.push("-");
                    semanticStack.push(op2);
                    arithOp_push();
                    Operand temp = (Operand) semanticStack.pop();
                    Operand resFirstWord = new Operand(mode, Int, temp.getValue());
                    code.add(VMCodeLine.lessThan(resFirstWord, immideiateZero, result));
                    break;
                case ">":
                    semanticStack.push(op1);
                    semanticStack.push("<=");
                    semanticStack.push(op2);
                    relationOp_push();
                    result = (Operand) semanticStack.pop();
                    code.add(VMCodeLine.logicalNot(result, result));

                    break;
                case "<=":
                    semanticStack.push(op1);
                    semanticStack.push("<");
                    semanticStack.push(op2);
                    relationOp_push();
                    Operand lessThanRes = (Operand) semanticStack.pop();
                    semanticStack.push(op1);
                    semanticStack.push("=");
                    semanticStack.push(op2);
                    Operand equalRes = (Operand) semanticStack.pop();
                    code.add(VMCodeLine.logicalOr(lessThanRes, equalRes, result));
                    break;
                case ">=":
                    semanticStack.push(op1);
                    semanticStack.push("<");
                    semanticStack.push(op2);
                    relationOp_push();
                    result = (Operand) semanticStack.pop();
                    code.add(VMCodeLine.logicalNot(result, result));
                    break;
            }
        } else {

            switch (operatorSym) {
                case "=":
                    code.add(VMCodeLine.equals(castedOp1, castedOp2, result));
                    break;
                case "!=":
                    code.add(VMCodeLine.notEquals(castedOp1, castedOp2, result));
                    break;
                case "<":
                    code.add(VMCodeLine.lessThan(castedOp1, castedOp2, result));
                    break;
                case ">":
                    code.add(VMCodeLine.greaterThan(castedOp1, castedOp2, result));
                    break;
                case "<=":
                    code.add(VMCodeLine.lessThanOrEqual(castedOp1, castedOp2, result));
                    break;
                case ">=":
                    code.add(VMCodeLine.greaterThanOrEqual(castedOp1, castedOp2, result));
                    break;
            }
        }
        semanticStack.push(result);

    }

    private void arithOp_push() throws CodeGeneratorException {
        Operand op2 = (Operand) semanticStack.pop();
        String operatorSym = (String) semanticStack.pop();
        Operand op1 = (Operand) semanticStack.pop();
        PrimitiveVariableType castedType = PrimitiveVariableType.getCastType(op1.getPrimitiveVariableType(), op2.getPrimitiveVariableType());
        AddressingMode mode = getAddressingModeInCode();
        if (castedType.equals(Int) ||
                castedType.equals(PrimitiveVariableType.Bool) ||
                castedType.equals(PrimitiveVariableType.Char)) {
            castedType = Int;
        } else if (castedType.equals(PrimitiveVariableType.Real)) {
            castedType = PrimitiveVariableType.Real;//Keep it real!
        } else { //LongInt
            castedType = PrimitiveVariableType.LongInt;
        }
        Operand castedOp1 = castCode(op1, castedType);
        Operand castedOp2 = castCode(op2, castedType);
        Operand result = getTempOperand(castedType, mode);


        if (castedType.equals(PrimitiveVariableType.LongInt)) {
            //TODO: long arithmatics
        } else {
            switch (operatorSym) {
                case "+":
                    code.add(VMCodeLine.add(castedOp1, castedOp2, result));
                    break;
                case "-":
                    code.add(VMCodeLine.subtract(castedOp1, castedOp2, result));
                    break;
                case "/":
                    code.add(VMCodeLine.divide(castedOp1, castedOp2, result));
                    break;
                case "*":
                    code.add(VMCodeLine.multiply(castedOp1, castedOp2, result));
                    break;
                case "%":
                    code.add(VMCodeLine.mod(castedOp1, castedOp2, result));
                    break;
            }
        }

        semanticStack.push(result);

    }

    private void unaryOp_push() throws CodeGeneratorException {
        Operand operand = (Operand) semanticStack.pop();
        String operatorSym = (String) semanticStack.pop();
        AddressingMode mode = getAddressingModeInCode();
        Operand castedOp, result;
        if (operatorSym.equals("!")) {
            castedOp = castCode(operand, PrimitiveVariableType.Bool);
            code.add(VMCodeLine.logicalNot(castedOp, castedOp));
            result = castedOp;
        } else if (operatorSym.equals("-")) {
            if (operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Real)) {
                code.add(VMCodeLine.unaryMinus(operand, operand));
                result = operand;
            } else if (operand.getPrimitiveVariableType().equals(PrimitiveVariableType.LongInt)) {
                Operand opFirstWord = new Operand(mode, Int, operand.getValue());
                Operand opSecondWord = new Operand(mode, Int, (int) operand.getValue() + 1);
                code.add(VMCodeLine.binaryNot(opFirstWord, opFirstWord));
                code.add(VMCodeLine.binaryNot(opSecondWord, opSecondWord));
                code.add(VMCodeLine.add(opSecondWord, immideiateOne, opSecondWord));
                Operand needCarry = getTempOperand(PrimitiveVariableType.Bool, mode);
                //if the second word is zero that means it was full 1 before adding one so we have carry
                code.add(VMCodeLine.equals(opSecondWord, immideiateZero, needCarry));
                Operand intNeedCarry = getTempOperand(Int, mode);
                code.add(VMCodeLine.add(opFirstWord, intNeedCarry, opFirstWord));
                result = operand;
            } else //all other cases must be cast to integer
            {
                castedOp = castCode(operand, Int);
                code.add(VMCodeLine.unaryMinus(castedOp, castedOp));
                result = castedOp;
            }
        } else // ~
        {
            if (operand.getPrimitiveVariableType().equals(PrimitiveVariableType.LongInt)) {
                Operand opFirstWord = new Operand(mode, Int, operand.getValue());
                Operand opSecondWord = new Operand(mode, Int, (int) operand.getValue() + 1);
                code.add(VMCodeLine.binaryNot(opFirstWord, opFirstWord));
                code.add(VMCodeLine.binaryNot(opSecondWord, opSecondWord));
                result = operand;
            } else {
                castedOp = castCode(operand, Int);
                code.add(VMCodeLine.logicalNot(castedOp, castedOp));
                result = castedOp;
            }

        }

        semanticStack.push(result);

    }

    /**
     * for long making immidiate operand is impossible
     * becuase we need it stored in two consecutive words
     * and immidiates arent even saved so we need to get a
     * memmory address for long types
     */
    private void makeConst_Push() throws DuplicateSymbolAddException {
        Object value = scanner.tokenValue();
        AddressingMode mode = getAddressingModeInCode();

        if (!(value instanceof String || value instanceof Long || value instanceof Character)) {
            semanticStack.push(getImmediateOperand(value));
        } else if (value instanceof Character) {
            Operand tempAsInt = getTempOperand(Int, mode);
            Operand tempAsChar = new Operand(mode, Char, tempAsInt.getValue());
            code.add(VMCodeLine.assign(getImmediateOperand(getAscii((Character) value)), tempAsInt));
            semanticStack.push(tempAsChar);
        } else if (value instanceof Long) {
            if ((Long) value <= Integer.MAX_VALUE && (Long) value >= Integer.MIN_VALUE) {
                semanticStack.push(getImmediateOperand(Math.toIntExact((Long) value)));
            } else {
                ArrayList words = longAsInts((Long) value);
                Operand result = getTempOperand(PrimitiveVariableType.LongInt, mode);
                Operand immediateFirstWord = getImmediateOperand(words.get(0));
                Operand immediateSecondWord = getImmediateOperand(words.get(1));
                Operand firstWord = new Operand(mode, Int, result.getValue());
                Operand secondWord = new Operand(mode, Int, (int) result.getValue() + 1);
                code.add(VMCodeLine.assign(immediateFirstWord, firstWord));
                code.add(VMCodeLine.assign(immediateSecondWord, secondWord));
                semanticStack.push(result);
            }
        } else { //String Constant
            String text = (String) scanner.tokenValue();
            AddressingMode inDirMode = getAddressingModeInDirectInCode(mode);

            Operand pointer = getTempOperand(Int, mode);
            Operand inDirPointer = getTempOperand(Int, inDirMode);
            Operand dirPointer = new Operand(mode, Int, inDirPointer.getValue());

            code.add(VMCodeLine.getMemory(getImmediateOperand(text.length() + 1), pointer));
            code.add(VMCodeLine.assign(pointer, dirPointer));

            VMCodeLine gotoNextChar = VMCodeLine.add(dirPointer, immideiateOne, dirPointer);
            for (int i = 0; i < text.length(); ++i) {
                code.add(VMCodeLine.assign(getImmediateOperand(getAscii(text.charAt(i))), inDirPointer));
                code.add(gotoNextChar);
            }
            code.add(VMCodeLine.assign(immideiateNullCharAsInt, inDirPointer));
            pointer.setStringPointer(true);
            semanticStack.push(pointer);
        }
    }

    private Operand castToBooleanCode(Operand operand) throws DuplicateSymbolAddException {

        AddressingMode mode = getAddressingModeInCode();

        if (operand.getPrimitiveVariableType().equals(Int) ||
                operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Real) ||
                operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Char)) {
            Operand operandBitPattern = new Operand(mode, Int, operand.getValue());
            Operand CastedOperand = getTempOperand(PrimitiveVariableType.Bool, mode);
            code.add(VMCodeLine.notEquals(operandBitPattern, immideiateZero, CastedOperand));
            return CastedOperand;
        } else if (operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Bool)) {
            return operand;
        } else { //this case is for long int
            if (operand.getAddressingMode().isIndirect()) {
                operand = assignLongToDirect(operand);
            }

            Operand firstWord = new Operand(operand.getAddressingMode(), Int, operand.getValue());
            Operand secondWord = new Operand(operand.getAddressingMode(), Int, (int) operand.getValue() + 1);
            Operand castedFirstWord = castToBooleanCode(firstWord);
            Operand castedSecondWord = castToBooleanCode(secondWord);
            Operand finalRes = getTempOperand(PrimitiveVariableType.Bool, mode);
            code.add(VMCodeLine.assign(castedFirstWord, finalRes));
            code.add(VMCodeLine.jumpZero(firstWord, new Operand(AddressingMode.Immidiate, Int, getNextLineAddress() + 2)));
            code.add(VMCodeLine.jump(new Operand(AddressingMode.Immidiate, Int, getNextLineAddress() + 2)));
            code.add(VMCodeLine.assign(finalRes, castedSecondWord));
            code.add(VMCodeLine.binaryOr(finalRes, castedFirstWord, finalRes));
            return finalRes;
        }

    }

    private Operand assignLongToDirect(Operand operand) throws DuplicateSymbolAddException {
        AddressingMode mode = getAddressingModeInCode();
        Operand directLong = getTempOperand(LongInt, mode);
        Operand firstDir = new Operand(operand.getAddressingMode(), Int, directLong.getValue());
        Operand secondDir = new Operand(operand.getAddressingMode(), Int, (int) directLong.getValue() + 1);
        code.add(VMCodeLine.assign(operand, firstDir));
        Operand operandDir = new Operand(getAddressingModeInCode(), Int, operand.getValue());
        code.add(VMCodeLine.add(operandDir, immideiateOne, operandDir));
        code.add(VMCodeLine.assign(operand, secondDir));
        code.add(VMCodeLine.subtract(operandDir, immideiateOne, operandDir));
        operand = directLong;
        return operand;
    }

    private Operand castToCharCode(Operand operand) throws CodeGeneratorException {

        AddressingMode mode = getAddressingModeInCode();
        Operand castedOperand;
        if (operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Bool) ||
                operand.getPrimitiveVariableType().equals(Int)) {
            castedOperand = getTempOperand(PrimitiveVariableType.Char, mode);
            code.add(VMCodeLine.assign(operand, castedOperand));
            return castedOperand;
        } else if (operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Char)) {
            return operand;
        } else if (operand.getPrimitiveVariableType().equals(PrimitiveVariableType.LongInt)) {
            if (operand.getAddressingMode().isIndirect()) {
                operand = assignLongToDirect(operand);
            }
            castedOperand = getTempOperand(PrimitiveVariableType.Char, mode);
            Operand secondWord = new Operand(operand.getAddressingMode(), Int, (int) operand.getValue() + 1);
            code.add(VMCodeLine.assign(secondWord, castedOperand));
            return castToCharCode(operand);
        }
        // this is case for real number
        else {
            Operand temp = castCode(operand, Int);
            return castCode(temp, PrimitiveVariableType.Char);
        }

    }



    private Operand castToIntegerCode(Operand operand) throws DuplicateSymbolAddException {

        AddressingMode mode = getAddressingModeInCode();

        if (operand.getPrimitiveVariableType().equals(Int)) {
        return operand;

        } else if(operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Real)) {
            Operand halfReal = new Operand(Immidiate, Real, 0.5);
            Operand roundOperand = getTempOperand(Real, mode);
            code.add(VMCodeLine.add(operand, halfReal, roundOperand));

            Operand CastedOperand = getTempOperand(Int, mode);
            Operand zero = getImmediateOperand(0);
            code.add(VMCodeLine.assign(zero, CastedOperand));

            Operand operandSignBit = getTempOperand(PrimitiveVariableType.Int, mode);
            Operand neg = getImmediateOperand(-2147483648);
            code.add(VMCodeLine.binaryAnd(new Operand(mode, Int, operand.getValue()), neg, operandSignBit));

            Operand halfRealCheckDest = new Operand(AddressingMode.Immidiate, PrimitiveVariableType.Int, getNextLineAddress());
            code.add(VMCodeLine.jumpZero(operandSignBit, halfRealCheckDest));
            code.add(VMCodeLine.subtract(roundOperand, halfReal, roundOperand));
            code.add(VMCodeLine.subtract(roundOperand, halfReal, roundOperand));

            halfRealCheckDest.setValue(getNextLineAddress());

            Operand exponent = getTempOperand(PrimitiveVariableType.Int, mode);
            Operand twentyThree = getImmediateOperand(23);
            Operand exponentMaxOperand = getImmediateOperand(255);
            code.add(VMCodeLine.assign(roundOperand, exponent));
            code.add(VMCodeLine.logicalRightShift(twentyThree, exponent));
            code.add(VMCodeLine.binaryAnd(exponent, exponentMaxOperand, exponent));

            Operand maxMantissa = getImmediateOperand(8388607);
            Operand hiddenOne = getImmediateOperand(8388608);
            Operand mantissa = getTempOperand(PrimitiveVariableType.Int, mode);
            code.add(VMCodeLine.binaryAnd(new Operand(mode, Int, roundOperand.getValue()), maxMantissa, mantissa));
            code.add(VMCodeLine.binaryOr(mantissa, hiddenOne, mantissa));

            Operand jumpZeroDestination = new Operand(AddressingMode.Immidiate, PrimitiveVariableType.Int, getNextLineAddress());
            code.add(VMCodeLine.jumpZero(exponent, jumpZeroDestination));

            Operand rightShift = getTempOperand(PrimitiveVariableType.Int, mode);
            Operand leftShift = getTempOperand(PrimitiveVariableType.Int, mode);
            Operand shiftBias = getImmediateOperand(150);
            code.add(VMCodeLine.subtract(shiftBias, exponent, rightShift));
            code.add(VMCodeLine.unaryMinus(rightShift, leftShift));

            code.add(VMCodeLine.assign(mantissa, CastedOperand));
            code.add(VMCodeLine.logicalRightShift(rightShift, CastedOperand));
            code.add(VMCodeLine.logicalLeftShift(leftShift, CastedOperand));

            code.add(VMCodeLine.jumpZero(operandSignBit, jumpZeroDestination));
            code.add(VMCodeLine.unaryMinus(CastedOperand, CastedOperand));

            jumpZeroDestination.setValue(getNextLineAddress());
            return CastedOperand;
        }  else if (operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Char) || operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Bool)) {
            Operand CastedOperand = getTempOperand(Int, mode);
            code.add(VMCodeLine.assign(operand, CastedOperand));
            return CastedOperand;

        } else { //this case is for long int

            if (operand.getAddressingMode().isIndirect()) {
                operand = assignLongToDirect(operand);
            }

            Operand firstWord = new Operand(operand.getAddressingMode(), Int, operand.getValue());
            Operand castedOperand = getTempOperand(Int, mode);
            code.add(VMCodeLine.assign(firstWord, castedOperand));
            Operand cdn = getTempOperand(Bool, mode);
            code.add(VMCodeLine.greaterThanOrEqual(operand, immideiateZero, cdn));
            Operand jumpAdr = new Operand(AddressingMode.Immidiate, Int, -1);
            code.add(VMCodeLine.jumpZero(cdn, jumpAdr));
            code.add(VMCodeLine.binaryNot(castedOperand, castedOperand));
            code.add(VMCodeLine.add(castedOperand, immideiateOne, castedOperand));
            jumpAdr.setValue(getNextLineAddress());
            return castedOperand;

        }

    }

    private Operand castToRealCode(Operand operand) throws CodeGeneratorException {

        AddressingMode mode = getAddressingModeInCode();

        if (operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Real)) {
            return operand;

        } else if(operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Bool)) {

            Operand castedOperand = getTempOperand(PrimitiveVariableType.Real, mode);
            Operand zero = immideiateZero;
            Operand lastBitMask = getImmediateOperand(1065353216);
            code.add(VMCodeLine.assign(zero, castedOperand));
            Operand jumpZeroDestination = new Operand(AddressingMode.Immidiate, Int, getNextLineAddress() + 2);
            code.add(VMCodeLine.jumpZero(operand, jumpZeroDestination));
            code.add(VMCodeLine.assign(lastBitMask, castedOperand));
            return castedOperand;

        } else if (operand.getPrimitiveVariableType().equals(Int)) {

            Operand castedOperand = getTempOperand(PrimitiveVariableType.Int, mode);
            Operand zero = immideiateZero;
            Operand one = immideiateOne;
            code.add(VMCodeLine.assign(zero, castedOperand));
            Operand jumpZeroDestination1 = new Operand(AddressingMode.Immidiate, Int, getNextLineAddress());
            code.add(VMCodeLine.jumpZero(operand, jumpZeroDestination1));

            Operand signBit = getTempOperand(Int, mode);
            Operand signBitMax = getImmediateOperand(-2147483648);
            code.add(VMCodeLine.binaryAnd(operand, signBitMax, signBit));
            Operand abs = getTempOperand(Int, mode);
            code.add(VMCodeLine.assign(operand, abs));
            Operand lt = getTempOperand(PrimitiveVariableType.Bool, mode);
            code.add(VMCodeLine.lessThan(operand, zero, lt));
            Operand jumpZeroDestination2 = new Operand(AddressingMode.Immidiate, Int, getNextLineAddress());
            code.add(VMCodeLine.jumpZero(lt, jumpZeroDestination2));
            code.add(VMCodeLine.unaryMinus(operand, abs));
            jumpZeroDestination2.setValue(getNextLineAddress());

            Operand exponentDefaultValue = getImmediateOperand(158);
            Operand exponent = getTempOperand(PrimitiveVariableType.Int, mode);
            code.add(VMCodeLine.assign(exponentDefaultValue, exponent));
            Operand operandSignBit = getTempOperand(Int, mode);
            Operand jumpZeroDestination3 = new Operand(AddressingMode.Immidiate, Int, getNextLineAddress());
            code.add(VMCodeLine.subtract(exponent, one, exponent));
            code.add(VMCodeLine.logicalLeftShift(one, abs));
            code.add(VMCodeLine.binaryAnd(abs, signBitMax, operandSignBit));
            Operand whileCondition = castCode(operandSignBit, PrimitiveVariableType.Bool);
            code.add(VMCodeLine.jumpZero(whileCondition, jumpZeroDestination3));
            code.add(VMCodeLine.logicalLeftShift(one, abs));
            Operand mantissa = getTempOperand(Int, mode);
            Operand eight = getImmediateOperand(9);
            code.add(VMCodeLine.logicalRightShift(eight, abs));
            code.add(VMCodeLine.assign(abs, mantissa));

            Operand twentyThree = getImmediateOperand(23);
            Operand maxMantissa = getImmediateOperand(8388607);
            code.add(VMCodeLine.logicalLeftShift(twentyThree, exponent));
            code.add(VMCodeLine.binaryAnd(mantissa, maxMantissa, mantissa));

            code.add(VMCodeLine.binaryOr(castedOperand, signBit, castedOperand));
            code.add(VMCodeLine.binaryOr(castedOperand, exponent, castedOperand));
            code.add(VMCodeLine.binaryOr(castedOperand, mantissa, castedOperand));
            jumpZeroDestination1.setValue(getNextLineAddress());
            return new Operand(mode, Real, castedOperand.getValue());

        } else if(operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Char)) {

            Operand castedToInt = castCode(operand, Int);
            Operand castedOperand = castCode(castedToInt, PrimitiveVariableType.Real);
            return castedOperand;

        } else { //this case is for long int

            if (operand.getAddressingMode().isIndirect()) {
                operand = assignLongToDirect(operand);
            }

            Operand castedOperand = getTempOperand(PrimitiveVariableType.Real, mode);
            Operand zero = immideiateZero;
            Operand one = immideiateOne;
            code.add(VMCodeLine.assign(zero, castedOperand));
            Operand jumpZeroDestination1 = new Operand(AddressingMode.Immidiate, Int, getNextLineAddress());
            code.add(VMCodeLine.jumpZero(operand, jumpZeroDestination1));

            Operand signBit = getTempOperand(Int, mode);
            code.add(VMCodeLine.assign(zero, signBit));
            Operand abs = getTempOperand(PrimitiveVariableType.LongInt, mode);
            longAssign(operand, abs);

            Operand lt = longLessThan(operand, zero);
            Operand jumpZeroDestination2 = new Operand(AddressingMode.Immidiate, Int, getNextLineAddress());
            code.add(VMCodeLine.jumpZero(lt, jumpZeroDestination2));
            Operand neg = getImmediateOperand(-2147483648);
            code.add(VMCodeLine.assign(neg, signBit));
            code.add(VMCodeLine.unaryMinus(operand, abs));
            jumpZeroDestination2.setValue(getNextLineAddress());

            Operand exponentDefaultValue = getImmediateOperand(190);
            Operand exponent = getTempOperand(PrimitiveVariableType.Int, mode);
            code.add(VMCodeLine.assign(exponentDefaultValue, exponent));
            Operand jumpZeroDestination3 = new Operand(AddressingMode.Immidiate, Int, getNextLineAddress());
            code.add(VMCodeLine.subtract(exponent, one, exponent));
            shiftLongOp(abs, (int) one.getValue(), true);
            Operand operandSignBit = getTempOperand(Int, mode);
            code.add(VMCodeLine.binaryAnd(abs, neg, operandSignBit));
            Operand whileCondition = castCode(operandSignBit, PrimitiveVariableType.Bool);
            code.add(VMCodeLine.jumpZero(whileCondition, jumpZeroDestination3));
            //
            Operand mantissa = getTempOperand(Int, mode);
            Operand eight = getImmediateOperand(8);
            Operand firstWord = new Operand(mode, Int, abs.getValue());
            code.add(VMCodeLine.logicalRightShift(eight, firstWord));
            code.add(VMCodeLine.assign(firstWord, mantissa));

            Operand twentyThree = getImmediateOperand(23);
            Operand maxMantissa = getImmediateOperand(8388607);
            code.add(VMCodeLine.logicalLeftShift(twentyThree, exponent));
            code.add(VMCodeLine.binaryAnd(mantissa, maxMantissa, mantissa));

            code.add(VMCodeLine.binaryOr(castedOperand, signBit, castedOperand));
            code.add(VMCodeLine.binaryOr(castedOperand, exponent, castedOperand));
            code.add(VMCodeLine.binaryOr(castedOperand, mantissa, castedOperand));
            jumpZeroDestination1.setValue(getNextLineAddress());
            return castedOperand;
        }

    }

    private void longAssign(Operand first, Operand result) {
        Operand firstWord = new Operand(first.getAddressingMode(), Int, first.getValue());
        Operand secondWord = new Operand(first.getAddressingMode(), Int, (int) first.getValue() + 1);
        Operand resultFirstWord = new Operand(result.getAddressingMode(), Int, result.getValue());
        Operand resultSecondWord = new Operand(result.getAddressingMode(), Int, (int) result.getValue() + 1);
        code.add(VMCodeLine.assign(firstWord, resultFirstWord));
        code.add(VMCodeLine.assign(secondWord, resultSecondWord));
    }

    private Operand longLessThan(Operand first, Operand second) throws CodeGeneratorException {
        semanticStack.push(first);
        semanticStack.push("<");
        semanticStack.push(second);
        relationOp_push();
        Operand lt = (Operand) semanticStack.pop();
        return lt;
    }

    private Operand getTempOperand(PrimitiveVariableType type, AddressingMode mode) throws DuplicateSymbolAddException {
        String operandName = getNextTempSymbol();
        int address = getFirstEmptyAdrInScope();
        PrimitiveVariableDescriptor pvd = new PrimitiveVariableDescriptor(address, type);
        activityRecord.addSymbol(operandName, pvd);
        Operand newOperand = new Operand(mode, type, address);
        return newOperand;
    }

    private Operand getImmediateOperand(Object value, PrimitiveVariableType type) {
        return new Operand(AddressingMode.Immidiate, type, value);
    }


    private Operand castToLongCode(Operand operand) throws CodeGeneratorException {

        AddressingMode mode = getAddressingModeInCode();
        Operand castedOperand;
        if (operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Bool) ||
                operand.getPrimitiveVariableType().equals(PrimitiveVariableType.Char)) {
            castedOperand = getTempOperand(PrimitiveVariableType.LongInt, mode);
            Operand secondWord = new Operand(castedOperand.getAddressingMode(), Int, (int) castedOperand.getValue() + 1);
            code.add(VMCodeLine.assign(operand, secondWord));
            return castedOperand;
        } else if (operand.getPrimitiveVariableType().equals(Int)) {
            castedOperand = getTempOperand(PrimitiveVariableType.LongInt, mode);
            Operand cdn = getTempOperand(PrimitiveVariableType.Bool, mode);
            Operand firstWord = new Operand(castedOperand.getAddressingMode(), Int, castedOperand.getValue());
            Operand secondWord = new Operand(castedOperand.getAddressingMode(), Int, (int) castedOperand.getValue() + 1);
            code.add(VMCodeLine.assign(operand, secondWord));
            code.add(VMCodeLine.lessThan(operand, immideiateZero, cdn));
            code.add(VMCodeLine.jumpZero(cdn, getImmediateOperand(getNextLineAddress() + 2)));
            code.add(VMCodeLine.assign(getImmediateOperand(fullOneInt), firstWord));
            return castedOperand;
        } else if (operand.getPrimitiveVariableType().equals(PrimitiveVariableType.LongInt)) {
            if (operand.getAddressingMode().isIndirect()) {
                operand = assignLongToDirect(operand);
            }
            return operand;
        }
        //this case is for real number
        else {
            castedOperand = getTempOperand(PrimitiveVariableType.LongInt, mode);
            Operand secondWord = new Operand(castedOperand.getAddressingMode(), Int, (int) castedOperand.getValue() + 1);
            code.add(VMCodeLine.assign(operand, secondWord));
            return castedOperand;
        }


    }


    private Operand castCode(Operand operand, PrimitiveVariableType castTo) throws CodeGeneratorException {
        if (operand.isStringPointer()) {
            throw new IncompatibleVariableTypeException();
        }
        AddressingMode mode = getAddressingModeInCode();
        if (operand.getAddressingMode().equals(AddressingMode.Immidiate)) {
            if (operand.getPrimitiveVariableType().equals(LongInt)) {
                Operand temp = getTempOperand(LongInt, mode);
                Operand firstWord = new Operand(mode, Int, temp.getValue());
                Operand secondWord = new Operand(mode, Int, 1 + (int) temp.getValue());
                ArrayList<Integer> longAsInts = longAsInts((Long) operand.getValue());
                code.add(VMCodeLine.assign(getImmediateOperand(longAsInts.get(0)), firstWord));
                code.add(VMCodeLine.assign(getImmediateOperand(longAsInts.get(1)), secondWord));
                operand = temp;
            } else {
                Operand temp = getTempOperand(operand.getPrimitiveVariableType(), mode);
                code.add(VMCodeLine.assign(operand, temp));
                operand = temp;
            }
        }

        if (castTo.equals(PrimitiveVariableType.Bool)) {
            return castToBooleanCode(operand);
        } else if (castTo.equals(PrimitiveVariableType.Char)) {
            return  castToCharCode(operand);
        } else if (castTo.equals(PrimitiveVariableType.Real)) {
            return castToRealCode(operand);
        } else if (castTo.equals(PrimitiveVariableType.LongInt)) {
            return  castToLongCode(operand);
        }
        // castTo == Int
        else {
            return castToIntegerCode(operand);
        }
    }

    private AddressingMode getAddressingModeInCode() {
        if (location.equals(CodeLocation.Global)) {
            return GlobalDirect;
        }
        if (location.equals(CodeLocation.Environment)) {
            if (locationStack.size() == 0 || locationStack.peek().equals(CodeLocation.Global)) {
                return GlobalDirect;
            }
        }
        return LocalDirect;
    }

    private void strlen_push() throws DuplicateSymbolAddException {

        Operand pointerToString = (Operand) semanticStack.pop();

        AddressingMode mode = getAddressingModeInCode();
        AddressingMode inDirMode = getAddressingModeInDirectInCode(mode);

        Operand len = getTempOperand(Int, mode);
        Operand inDirPointer = getTempOperand(Int, inDirMode);
        Operand dirPointer = new Operand(mode, Int, inDirPointer.getValue());
        Operand cdn = getTempOperand(PrimitiveVariableType.Bool, mode);


        code.add(VMCodeLine.assign(pointerToString, dirPointer));
        code.add(VMCodeLine.assign(immideiateZero, len));
        Operand backJumpAdr = getImmediateOperand(getNextLineAddress());
        Operand forwardJumpAdr = getImmediateOperand(-1);
        code.add(VMCodeLine.notEquals(inDirPointer, immideiateNullCharAsInt, cdn));
        code.add(VMCodeLine.jumpZero(cdn, forwardJumpAdr));

        VMCodeLine gotoNextChar = VMCodeLine.add(dirPointer, immideiateOne, dirPointer);
        VMCodeLine incLen = VMCodeLine.add(len, immideiateOne, len);
        code.add(incLen);
        code.add(gotoNextChar);
        code.add(VMCodeLine.jump(backJumpAdr));
        forwardJumpAdr.setValue(getNextLineAddress());

        semanticStack.push(len);
    }

    private void concat_push() throws DuplicateSymbolAddException {
        AddressingMode mode = getAddressingModeInCode();
        AddressingMode inDirMode = getAddressingModeInDirectInCode(mode);

        Operand pointerToSecond = (Operand) semanticStack.pop();
        Operand pointerToFirst = (Operand) semanticStack.pop();
        Operand pointerToRes = getTempOperand(Int, mode);
        Operand inDirPointer = getTempOperand(Int, inDirMode);
        Operand dirPointer = new Operand(mode, Int, inDirPointer.getValue());
        Operand inDirPointerRes = getTempOperand(Int, inDirMode);
        Operand dirPointerRes = new Operand(mode, Int, inDirPointerRes.getValue());
        Operand cdn = getTempOperand(PrimitiveVariableType.Bool, mode);

        semanticStack.push(pointerToFirst);
        strlen_push();
        Operand lenFirst = (Operand) semanticStack.pop();

        semanticStack.push(pointerToSecond);
        strlen_push();
        Operand lenSecond = (Operand) semanticStack.pop();

        Operand lenRes = getTempOperand(Int, mode);

        code.add(VMCodeLine.add(lenFirst, lenSecond, lenRes));
        code.add(VMCodeLine.add(lenRes, immideiateOne, lenRes));
        code.add(VMCodeLine.getMemory(lenRes, pointerToRes));
        code.add(VMCodeLine.assign(pointerToRes, dirPointerRes));

        //copying first string to concatination
        code.add(VMCodeLine.assign(pointerToFirst, dirPointer));


        Operand backJumpAdr = getImmediateOperand(getNextLineAddress());
        Operand forwardJump = getImmediateOperand(-1);
        code.add(VMCodeLine.notEquals(inDirPointer, immideiateNullCharAsInt, cdn));
        code.add(VMCodeLine.jumpZero(cdn, forwardJump));
        code.add(VMCodeLine.assign(inDirPointer, inDirPointerRes));
        code.add(VMCodeLine.add(dirPointer, immideiateOne, dirPointer));
        code.add(VMCodeLine.add(dirPointerRes, immideiateOne, dirPointerRes));
        code.add(VMCodeLine.jump(backJumpAdr));
        forwardJump.setValue(getNextLineAddress());

        //copying second string to concat
        code.add(VMCodeLine.assign(pointerToSecond, dirPointer));

        backJumpAdr = getImmediateOperand(getNextLineAddress());
        forwardJump = getImmediateOperand(-1);
        code.add(VMCodeLine.notEquals(inDirPointer, immideiateNullCharAsInt, cdn));
        code.add(VMCodeLine.jumpZero(cdn, forwardJump));
        code.add(VMCodeLine.assign(inDirPointer, inDirPointerRes));
        code.add(VMCodeLine.add(dirPointer, immideiateOne, dirPointer));
        code.add(VMCodeLine.add(dirPointerRes, immideiateOne, dirPointerRes));
        code.add(VMCodeLine.jump(backJumpAdr));
        forwardJump.setValue(getNextLineAddress());
        code.add(VMCodeLine.assign(immideiateNullCharAsInt, inDirPointer));

        pointerToRes.setStringPointer(true);
        semanticStack.push(pointerToRes);
    }

    private void computeInEnvOperand_push() throws NonExistentSymbolException,
            NonExistentVariableException, DuplicateSymbolAddException {
        Operand result;
        AddressingMode mode = getAddressingModeInCode();

        String envName = (String) semanticStack.pop();
        String varName = scanner.matchedText();
        EnvironmentDescriptor envDesc = (EnvironmentDescriptor) activityRecord.getSymbolDecriptor(envName);
        VariableDescriptor varDesc = (VariableDescriptor) envDesc.getSymbolDescriptor(varName);
        int address = varDesc.getAddress();
        result = getOperandBasedOnVariableType(mode, varDesc, address);
        if (activityRecord.isOnlyInGlobal(envName)) {
            result.makeGlobal();
        }

        if (varDesc instanceof StringVariableDescriptor) result.setStringPointer(true);

        semanticStack.push(result);
    }

    private Operand getOperandBasedOnVariableType(AddressingMode mode, VariableDescriptor varDesc, int address) {
        Operand result = null;
        switch (varDesc.getVariableType()) {
            case Array:
                result = new Operand(mode, Int, address);
                semanticStack.push(varDesc);
                break;
            case Structure:
                result = new Operand(mode, Int, address);
                semanticStack.push(varDesc);
                break;
            case String:
                result = new Operand(mode, Int, address);
                result.setStringPointer(true);
                break;
            case Primitive:
                PrimitiveVariableDescriptor pvd = (PrimitiveVariableDescriptor) varDesc;
                result = new Operand(mode, pvd.getPvt(), address);
                break;
        }
        return result;
    }

    private void pushLastDefinedIdOp() {
        VariableDescriptor vd = (VariableDescriptor) lastDefinedIDDesc;
        Operand result;
        if (vd.getVariableType().equals(Array)) {
            semanticStack.push(lastDefinedIDDesc);
            result = new Operand(getAddressingModeInCode(), Int, vd.getAddress());

        } else if (vd.getVariableType().equals(Structure)) {
            semanticStack.push(lastDefinedIDDesc);
            result = new Operand(getAddressingModeInCode(), Int, vd.getAddress());

        } else if (vd.getVariableType().equals(String)) {
            result = new Operand(getAddressingModeInCode(), Int, vd.getAddress());
            result.setStringPointer(true);
        }
        // case for primitive variable
        else {
            PrimitiveVariableDescriptor pvd = (PrimitiveVariableDescriptor) vd;
            result = new Operand(getAddressingModeInCode(), pvd.getPvt(), vd.getAddress());
        }

        semanticStack.push(result);
    }

    private void computeInStructOperand_Push() throws NonExistentSymbolException, NonExistentVariableException, DuplicateSymbolAddException {
        AddressingMode mode = getAddressingModeInCode();

        Operand data = getTempOperand(Int, mode);
        Operand structVarOp = (Operand) semanticStack.pop();
        StructureVariableDescriptor structVarDesc = (StructureVariableDescriptor) semanticStack.pop();
        String varName = scanner.matchedText();
        StructureDescriptor structDesc = structVarDesc.getDataType();
        VariableDescriptor vd = structDesc.getSymbolDescriptor(varName);
        int relAdr = vd.getAddress();
        code.add(VMCodeLine.add(getImmediateOperand(relAdr), structVarOp, data));
        if (vd instanceof PrimitiveVariableDescriptor) {
            PrimitiveVariableType pvt = ((PrimitiveVariableDescriptor) vd).getPvt();
            data = new Operand(getAddressingModeInDirectInCode(mode), pvt, data.getValue());
        } else {
            data = new Operand(getAddressingModeInDirectInCode(mode), Int, data.getValue());
            if (vd instanceof StructureVariableDescriptor) {
                StructureVariableDescriptor svd = (StructureVariableDescriptor) vd;
                semanticStack.push(svd);
            }
        }
        if (structDesc.getSymbolDescriptor(varName) instanceof StringVariableDescriptor) {
            data.setStringPointer(true);
        }
        semanticStack.push(data);
    }


    private void computeOperandOrFunc_Push() throws NonExistentSymbolException {
        String varName = scanner.matchedText();
        try {
            Descriptor desc = activityRecord.getSymbolDecriptor(varName);
            if (desc instanceof VariableDescriptor) {
                VariableDescriptor varDesc = (VariableDescriptor) desc;
                Operand result;
                int address = varDesc.getAddress();
                result = getOperandBasedOnVariableType(activityRecord.getAddressingMod(varName), varDesc, address);
                semanticStack.push(result);
            } else {
                pushTokenValue();
            }

        } catch (NonExistentSymbolException e) {
            semanticStack.push(scanner.matchedText());
        }

    }


    private void computeOperand_Push() throws NonExistentSymbolException {
        String varName = scanner.matchedText();
        VariableDescriptor varDesc = (VariableDescriptor) activityRecord.getSymbolDecriptor(varName);
        int address = varDesc.getAddress();
        Operand result = getOperandBasedOnVariableType(activityRecord.getAddressingMod(varName), varDesc, address);
        semanticStack.push(result);
    }


    private void pushDelimiter() {
        semanticStack.push(delimiter);
    }

    /**
     * this methods pushes the indirect operand to element to semantic stack
     *
     * @throws DuplicateSymbolAddException
     */
    private void computeElementAdr_pushOp() throws CodeGeneratorException {
        AddressingMode mode = getAddressingModeInCode();
        AddressingMode inDirMode = getAddressingModeInDirectInCode(mode);
        ArrayList<Operand> adrInDim = new ArrayList<>();
        while (semanticStack.peek() != delimiter) {
            adrInDim.add((Operand) semanticStack.pop());//first adr in last element
        }
        //pops delimiter
        semanticStack.pop();
        Operand startAdr = (Operand) semanticStack.pop();
        ArrayVariableDescriptor arrayVariableDescriptor = (ArrayVariableDescriptor) semanticStack.pop();
        Operand elementAdrDir = getTempOperand(Int, mode);
        Operand element;
        if (arrayVariableDescriptor.getDataType().equals(Primitive)) {
            PrimitiveVariableType pvt = ((ArrayOfPrimitiveDescriptor) arrayVariableDescriptor).getPrimitiveVariableType();
            element = new Operand(inDirMode, pvt, elementAdrDir.getValue());
        } else {
            element = new Operand(inDirMode, Int, elementAdrDir.getValue());
        }
        Operand dimension = getTempOperand(Int, inDirMode);
        Operand dimensionAdrDir = new Operand(mode, Int, dimension.getValue());
        Operand multDims = getTempOperand(Int, mode);
        Operand temp = getTempOperand(Int, mode);
        Operand dimCount = getTempOperand(Int, mode);
        code.add(VMCodeLine.assign(startAdr, dimCount));
        Operand inDirDimCount = new Operand(getAddressingModeInDirectInCode(mode), Int, dimCount.getValue());
        code.add(VMCodeLine.assign(inDirDimCount, dimCount));

        code.add(VMCodeLine.assign(startAdr, temp));
        code.add(VMCodeLine.add(startAdr, dimCount, dimensionAdrDir));
        code.add(VMCodeLine.add(dimensionAdrDir, immideiateOne, elementAdrDir));
        code.add(VMCodeLine.assign(immideiateOne, multDims));
        if (arrayVariableDescriptor.getDataType().equals(Primitive)
                && ((ArrayOfPrimitiveDescriptor) arrayVariableDescriptor).getPrimitiveVariableType().equals(LongInt)) {
            code.add(VMCodeLine.add(multDims, immideiateOne, multDims));
        }
        for (int i = 0; i < adrInDim.size(); ++i) {
            code.add(VMCodeLine.multiply(adrInDim.get(i), multDims, temp));
            code.add(VMCodeLine.add(temp, elementAdrDir, elementAdrDir));
            code.add(VMCodeLine.multiply(multDims, dimension, multDims));
            code.add(VMCodeLine.subtract(dimensionAdrDir, immideiateOne, dimensionAdrDir));
        }
        if (arrayVariableDescriptor instanceof ArrayOfStructureVariableDescriptor) {
            StructureDescriptor stDesc = ((ArrayOfStructureVariableDescriptor) arrayVariableDescriptor).getStructureDescriptor();
            //Todo: change this if possible
            //solution is to push Structure descriptor before operand and change code
            StructureVariableDescriptor tof = new StructureVariableDescriptor(-1, stDesc);
            semanticStack.push(tof);
        }
        if (arrayVariableDescriptor instanceof ArrayOfStringDescriptor) {
            element.setStringPointer(true);
        }
        semanticStack.push(element);
    }


    private AddressingMode getAddressingModeInDirectInCode(AddressingMode mode) {
        return (mode.equals(AddressingMode.GlobalDirect)) ? AddressingMode.GlobalIndirect : AddressingMode.LocalIndirect;
    }


    private void isVoid_Push() throws DuplicateSymbolAddException {
        AddressingMode mode = getAddressingModeInCode();
        Operand id = (Operand) semanticStack.pop();
        if (semanticStack.peek() instanceof StructureVariableDescriptor ||
                semanticStack.peek() instanceof ArrayVariableDescriptor) {
            semanticStack.pop();
        }
        Operand result = getTempOperand(Bool, mode);

        code.add(VMCodeLine.equals(new Operand(id.getAddressingMode(), Int, id.getValue()), immideiateMinusOne, result));
        semanticStack.push(result);
    }


    private void arrayDefAssignment() throws DuplicateSymbolAddException {
        AddressingMode mode = getAddressingModeInCode();
        ArrayList<Operand> dimSize = new ArrayList<>();
        while (semanticStack.peek() != delimiter) {
            dimSize.add(0, (Operand) semanticStack.pop());
        }
        //pops delimiter
        semanticStack.pop();
        int dimCount = dimSize.size();
        Operand arrayOp = (Operand) semanticStack.pop();
        Operand tempDir = getTempOperand(Int, mode);
        Operand tempInDir = new Operand(getAddressingModeInDirectInCode(mode), Int, tempDir.getValue());
        ArrayVariableDescriptor desc = (ArrayVariableDescriptor) semanticStack.pop();
        Operand size = getTempOperand(Int, mode);
        code.add(VMCodeLine.assign(immideiateOne, size));
        if (desc.getDataType().equals(Primitive)
                && ((ArrayOfPrimitiveDescriptor) (desc)).getPrimitiveVariableType().equals(LongInt)) {
            code.add(VMCodeLine.add(size, immideiateOne, size));
        }
        for (Operand op : dimSize) {
            code.add(VMCodeLine.multiply(op, size, size));
        }
        code.add(VMCodeLine.add(size, getImmediateOperand(dimCount + 1), size));
        code.add(VMCodeLine.getMemory(size, arrayOp));
        //now arrayOp has address of array
        code.add(VMCodeLine.assign(arrayOp, tempDir));
        code.add(VMCodeLine.assign(getImmediateOperand(dimCount), tempInDir));
        for (int i = 0; i < dimSize.size(); ++i) {
            code.add(VMCodeLine.add(immideiateOne, tempDir, tempDir));
            code.add(VMCodeLine.assign(dimSize.get(i), tempInDir));
        }

        if (desc.getDataType().equals(Structure) || desc.getDataType().equals(String)) {
            Operand cdn = getTempOperand(Bool, mode);
            Operand jumpAdr = getImmediateOperand(-1);
            code.add(VMCodeLine.subtract(size, getImmediateOperand(1 + dimSize.size()), size));
            Operand backJump = getImmediateOperand(getNextLineAddress());
            code.add(VMCodeLine.greaterThanOrEqual(size, immideiateZero, cdn));
            code.add(VMCodeLine.jumpZero(cdn, jumpAdr));
            code.add(VMCodeLine.add(immideiateOne, tempDir, tempDir));
            code.add(VMCodeLine.assign(immideiateMinusOne, tempInDir));
            code.add(VMCodeLine.subtract(size, immideiateOne, size));
            code.add(VMCodeLine.jump(backJump));
            jumpAdr.setValue(getNextLineAddress());
        }

    }

    private void structDefAssignment() throws CodeGeneratorException {
        if (this.location.equals(CodeLocation.Structure)) {
            throw new noAssignInStructException();
        }
        AddressingMode mode = getAddressingModeInCode();
        ArrayList<Operand> operands = new ArrayList<>();
        while (semanticStack.peek() != delimiter) {

            if (semanticStack.peek().equals(emptyStructAssign)) {
                semanticStack.pop();
                Operand temp = (new Operand(mode, Int, 0));
                temp.setEmptyStructAssignment(true);
                operands.add(0, temp);
            } else {
                operands.add(0, (Operand) semanticStack.pop());
                if (semanticStack.peek() instanceof ArrayVariableDescriptor ||
                        semanticStack.peek() instanceof StructureVariableDescriptor) {
                    semanticStack.pop();
                }
            }
        }
        //pops delimiter
        semanticStack.pop();
        Operand strVarOp = (Operand) semanticStack.pop();
        Operand tempDir = getTempOperand(Int, mode);
        Operand tempInDir = new Operand(getAddressingModeInDirectInCode(mode), Int, tempDir.getValue());
        StructureDescriptor strDesc;
        StructureVariableDescriptor strVarDesc = (StructureVariableDescriptor) semanticStack.pop();
        strDesc = strVarDesc.getDataType();
        Operand cdn = getTempOperand(Bool, mode);
        code.add(VMCodeLine.notEquals(strVarOp, immideiateMinusOne, cdn));
        Operand jumpAdr = getImmediateOperand(-1);
        code.add(VMCodeLine.jumpZero(cdn, jumpAdr));
        code.add(VMCodeLine.freeMemory(strVarOp, getImmediateOperand(strDesc.getDataSize())));
        jumpAdr.setValue(getNextLineAddress());
        code.add(VMCodeLine.getMemory(getImmediateOperand(strDesc.getDataSize()), strVarOp));
        code.add(VMCodeLine.assign(strVarOp, tempDir));
        for (int i = 0; i < strDesc.getDataCount(); ++i) {
            if (operands.get(i).isEmptyStructAssignment()) {
                if (strDesc.getDescriptorByOrder(i).getVariableType().equals(Structure) ||
                        strDesc.getDescriptorByOrder(i).getVariableType().equals(String) ||
                        strDesc.getDescriptorByOrder(i).getVariableType().equals(Array)) {
                    code.add(VMCodeLine.assign(immideiateMinusOne, tempInDir));
                }
            } else {
                if (strDesc.getDescriptorByOrder(i).getVariableType().equals(Primitive)) {
                    PrimitiveVariableType pvt = ((PrimitiveVariableDescriptor) strDesc.getDescriptorByOrder(i)).getPvt();
                    Operand casted = castCode(operands.get(i), pvt);
                    if (pvt.equals(LongInt)) {
                        Operand firstWord = new Operand(mode, Int, casted.getValue());
                        Operand secondWord = new Operand(mode, Int, (int) casted.getValue() + 1);
                        code.add(VMCodeLine.assign(firstWord, tempInDir));
                        code.add(VMCodeLine.add(tempDir, immideiateOne, tempDir));
                        code.add(VMCodeLine.assign(secondWord, tempInDir));
                    } else {
                        code.add(VMCodeLine.assign(casted, new Operand(tempInDir.getAddressingMode(),
                                pvt, tempInDir.getValue())));
                    }
                } else {
                    code.add(VMCodeLine.assign(operands.get(i), tempInDir));
                }
            }
            code.add(VMCodeLine.add(tempDir, immideiateOne, tempDir));
        }
    }

    private void normalAssignment() throws CodeGeneratorException {
        //this is for tuple assignment
        if (this.location.equals(CodeLocation.Structure)) {
            throw new noAssignInStructException();
        }
        if (semanticStack.peek().equals(delimiter))
        {
            //pops Delimiter
            semanticStack.pop();
            ArrayList<Operand> rhsOperands = new ArrayList<>();
            ArrayList<VariableDescriptor> rhsDescriptors = new ArrayList<>();
            while (semanticStack.peek() != delimiter) {
                rhsOperands.add(0, (Operand) semanticStack.pop());
                if (semanticStack.peek() instanceof ArrayVariableDescriptor ||
                        semanticStack.peek() instanceof StructureVariableDescriptor) {
                    rhsDescriptors.add(0, (VariableDescriptor) semanticStack.pop());
                } else {
                    rhsDescriptors.add(0, null);
                }
            }
            //pops Delimiter
            semanticStack.pop();
            ArrayList<Operand> lhsOperands = new ArrayList<>();
            ArrayList<VariableDescriptor> lhsDescriptors = new ArrayList<>();
            while (semanticStack.peek() != delimiter) {
                lhsOperands.add(0, (Operand) semanticStack.pop());
                if (semanticStack.peek() instanceof ArrayVariableDescriptor ||
                        semanticStack.peek() instanceof StructureVariableDescriptor) {
                    lhsDescriptors.add(0, (VariableDescriptor) semanticStack.pop());
                } else {
                    lhsDescriptors.add(0, null);
                }
            }
            //pops Delimiter
            semanticStack.pop();
            if (lhsOperands.size() != rhsOperands.size()) {
                throw new IncompatibleVariableTypeException();
            }
            for (int i = 0; i < lhsOperands.size(); ++i) {
                nonTupleAssignmentCode(rhsOperands.get(i), lhsOperands.get(i), rhsDescriptors.get(i), lhsDescriptors.get(i));
            }
        } else {

            Operand rhs = (Operand) semanticStack.pop();
            if (semanticStack.peek() instanceof StructureVariableDescriptor) {
                StructureVariableDescriptor rhsDesc = (StructureVariableDescriptor) semanticStack.pop();
                Operand lhs = (Operand) semanticStack.pop();
                StructureVariableDescriptor lhsDesc = (StructureVariableDescriptor) semanticStack.pop();
                if (!lhsDesc.getDataType().equals(rhsDesc.getDataType())) {
                    throw new IncompatibleVariableTypeException();
                }
                code.add(VMCodeLine.assign(rhs, lhs));
            } else if (semanticStack.peek() instanceof ArrayVariableDescriptor) {
                semanticStack.pop();
                Operand lhs = (Operand) semanticStack.pop();
                semanticStack.pop();
                code.add(VMCodeLine.assign(rhs, lhs));
            }
            //this is for string and primitive variable type
            else {
                Operand lhs = (Operand) semanticStack.pop();

                if (rhs.isStringPointer()) {
                    if (!lhs.isStringPointer()) {
                        throw new IncompatibleVariableTypeException();
                    }
                    code.add(VMCodeLine.assign(rhs, lhs));
                } else {
                    Operand casted = castCode(rhs, lhs.getPrimitiveVariableType());
                    if (lhs.getPrimitiveVariableType().equals(LongInt)) {
                        Operand rhsInt = new Operand(casted.getAddressingMode(), Int, casted.getValue());
                        Operand lhsInt = new Operand(lhs.getAddressingMode(), Int, lhs.getValue());
                        code.add(VMCodeLine.assign(rhsInt, lhsInt));

                        Operand lhsSecondWord;
                        if (lhs.getAddressingMode().isDirect()) {
                            lhsSecondWord = new Operand(getAddressingModeInCode(), Int, (int) lhsInt.getValue() + 1);
                            code.add(VMCodeLine.assign(new Operand(getAddressingModeInCode(), Int, (int) rhsInt.getValue() + 1), lhsSecondWord));
                        } else {
                            Operand lhsIntDir = new Operand(getAddressingModeInCode(), Int, lhsInt.getValue());
                            code.add(VMCodeLine.add(immideiateOne, lhsIntDir, lhsIntDir));
                            code.add(VMCodeLine.assign(new Operand(getAddressingModeInCode(), Int, (int) rhsInt.getValue() + 1), lhsInt));
                        }

                    } else {
                        code.add(VMCodeLine.assign(casted, lhs));
                    }
                }
            }
        }
    }

    private void nonTupleAssignmentCode(Operand rhs, Operand lhs, VariableDescriptor rhsDescGen,
                                        VariableDescriptor lhsDescGen) throws CodeGeneratorException {
        if (rhsDescGen instanceof StructureVariableDescriptor) {
            StructureVariableDescriptor rhsDesc = (StructureVariableDescriptor) rhsDescGen;
            StructureVariableDescriptor lhsDesc = (StructureVariableDescriptor) lhsDescGen;
            if (!lhsDesc.getDataType().equals(rhsDesc.getDataType())) {
                throw new IncompatibleVariableTypeException();
            }
            code.add(VMCodeLine.assign(rhs, lhs));
        } else if (semanticStack.peek() instanceof ArrayVariableDescriptor) {
            code.add(VMCodeLine.assign(rhs, lhs));
        }
        //this is for string and primitive variable type
        else {
            if (rhs.isStringPointer()) {
                if (!lhs.isStringPointer()) {
                    throw new IncompatibleVariableTypeException();
                }
                code.add(VMCodeLine.assign(rhs, lhs));
            } else {
                Operand casted = castCode(rhs, lhs.getPrimitiveVariableType());
                if (lhs.getPrimitiveVariableType().equals(LongInt)) {
                    Operand rhsInt = new Operand(rhs.getAddressingMode(), Int, rhs.getValue());
                    Operand lhsInt = new Operand(rhs.getAddressingMode(), Int, rhs.getValue());
                    code.add(VMCodeLine.assign(rhsInt, lhsInt));
                    code.add(VMCodeLine.add(rhsInt, immideiateOne, rhsInt));
                    code.add(VMCodeLine.add(lhsInt, immideiateOne, lhsInt));
                    code.add(VMCodeLine.assign(rhsInt, lhsInt));
                    code.add(VMCodeLine.subtract(rhsInt, immideiateOne, rhsInt));
                    code.add(VMCodeLine.subtract(lhsInt, immideiateOne, lhsInt));
                } else {
                    code.add(VMCodeLine.assign(casted, lhs));
                }
            }
        }
    }


    private void callFunction(boolean pushOut) throws CodeGeneratorException {
        Operand inputPlaceDir = getTempOperand(Int, getAddressingModeInCode());
        Operand outPlaceDir = getTempOperand(Int, getAddressingModeInCode());
        ArrayList<Descriptor> inputDescriptor = new ArrayList<>();
        ArrayList<Operand> inputs = new ArrayList<>();
        while (!semanticStack.peek().equals(delimiter)) {
            inputs.add(0, (Operand) semanticStack.pop());
            if (semanticStack.peek() instanceof ArrayVariableDescriptor ||
                    semanticStack.peek() instanceof StructureVariableDescriptor) {
                inputDescriptor.add(0, (VariableDescriptor) semanticStack.pop());
            } else {
                inputDescriptor.add(0, null);
            }
        }
        //Pop delimiter
        semanticStack.pop();
        String functionName = (String) semanticStack.pop();
        functionName = functionName + inputs.size();
        FunctionDescriptor fd = (FunctionDescriptor) activityRecord.getSymbolDecriptor(functionName);
        code.add(VMCodeLine.getMemory(getImmediateOperand(fd.getInputSize()), inputPlaceDir));
        code.add(VMCodeLine.getMemory(getImmediateOperand(fd.getOutputSize()), outPlaceDir));
        Operand currentInput = getTempOperand(Int, LocalIndirect);
        Operand currentInputDir = new Operand(LocalDirect, Int, currentInput.getValue());
        //Code for copying inputs of function to allocated memory
        code.add(VMCodeLine.assign(inputPlaceDir, currentInputDir));
        for (int i = 0; i < inputs.size(); ++i) {
            VariableDescriptor vd = fd.getInputDescriptors().get(i);
            if (vd.getVariableType().equals(Array)) {
                if (!(inputDescriptor.get(i) instanceof ArrayVariableDescriptor)) {
                    throw new IncompatibleVariableTypeException();
                }
                code.add(VMCodeLine.assign(inputs.get(i), currentInput));
            } else if (vd.getVariableType().equals(Structure)) {
                if (!(inputDescriptor.get(i) instanceof StructureVariableDescriptor)) {
                    throw new IncompatibleVariableTypeException();
                }
                code.add(VMCodeLine.assign(inputs.get(i), currentInput));
            } else if (vd.getVariableType().equals(String)) {
                if (!(inputs.get(i).isStringPointer())) {
                    throw new IncompatibleVariableTypeException();
                }
                code.add(VMCodeLine.assign(inputs.get(i), currentInput));
            }
            //Primitive
            else {
                if (inputDescriptor.get(i) != null || inputs.get(i).isStringPointer()) {
                    throw new IncompatibleVariableTypeException();
                }
                PrimitiveVariableType functionInput =
                        ((PrimitiveVariableDescriptor) fd.getInputDescriptors().get(i)).getPvt();
                Operand casted = castCode(inputs.get(i), functionInput);
                code.add(VMCodeLine.assign(casted, new Operand(LocalIndirect, functionInput,
                        currentInputDir.getValue())));
            }
            code.add(VMCodeLine.add(immideiateOne, currentInputDir, currentInputDir));
        }
        //OK huff
        // right now we copied input now we should push data to stack and jmp to function code
        Operand spInStack = new Operand(LocalDirect, Int,
                FunctionDescriptor.getPreviousStackPointerAddressRelativeToCallerStackPointer());
        Operand outPointerInStack = new Operand(LocalDirect, Int,
                FunctionDescriptor.getOutputAddressAddressRelativeToCallerStackPointer());
        Operand inPointerInStack = new Operand(LocalDirect, Int,
                FunctionDescriptor.getInputDataAddressAddressRelativeToCallerStackPointer());
        Operand retAdrInStack = new Operand(LocalDirect, Int,
                FunctionDescriptor.getReturnAddressAddressRelativeToCallerStackPointer());
        Operand jumpBack = getImmediateOperand(-1);
        //generating codes for jumping to function
        code.add(VMCodeLine.getSP(spInStack));
        code.add(VMCodeLine.assign(outPlaceDir, outPointerInStack));
        code.add(VMCodeLine.assign(inputPlaceDir, inPointerInStack));
        code.add(VMCodeLine.assign(jumpBack, retAdrInStack));
        code.add(VMCodeLine.decreaseSP(fd.getDataSizeImmidiateOperand()));
        code.add(VMCodeLine.jump(getImmediateOperand(fd.getStartAddress())));
        jumpBack.setValue(getNextLineAddress());
        code.add(VMCodeLine.increaseSP(fd.getDataSizeImmidiateOperand()));
        code.add(VMCodeLine.freeMemory(inputPlaceDir, getImmediateOperand(fd.getInputSize())));
        //Now we should copy outputs of function and free memory
        if (fd.getOutputCount() > 0) {
            Operand currentOutputInDir = getTempOperand(Int, AddressingMode.LocalIndirect);
            Operand currentOutputDir = new Operand(LocalDirect, Int, currentOutputInDir.getValue());
            code.add(VMCodeLine.assign(outPlaceDir, currentOutputDir));
            for (int i = 0; i < fd.getOutputCount(); ++i) {
                VariableDescriptor vd = fd.getOutputDescriptors().get(i);
                if (vd.getVariableType().equals(Primitive)) {
                    PrimitiveVariableType pvt = ((PrimitiveVariableDescriptor) vd).getPvt();
                    Operand tempOut = getTempOperand(pvt, getAddressingModeInCode());
                    code.add(VMCodeLine.assign(new Operand(LocalIndirect, pvt, currentOutputDir.getValue()), tempOut));
                    if (pushOut) {
                        semanticStack.push(tempOut);
                    }
                }
                //Non Primitives are pointer to data but the addressing mode is direct
                else {
                    Operand tempOut = getTempOperand(Int, getAddressingModeInCode());
                    code.add(VMCodeLine.assign(currentOutputInDir, tempOut));
                    if (pushOut) {
                        semanticStack.push(tempOut);
                    }
                }
                code.add(VMCodeLine.add(currentOutputDir, immideiateOne, currentOutputDir));
            }
        }
        //Delimiter for tuple assignment if function has multiple outputs
        if (fd.getOutputCount() > 1) {
            if (pushOut) {
                semanticStack.push(delimiter);
            }
        }
        code.add(VMCodeLine.freeMemory(outPlaceDir, getImmediateOperand(fd.getOutputSize())));
    }

    private void initReturn() {
        this.noReturn = false;
        FunctionDescriptor fd = this.lastFunction;
        if (fd.getOutputCount().equals(0)) {
            code.add(VMCodeLine.jump(fd.getReturnAdr()));
        } else {
            this.problematicReturn = true;
        }
    }

    private void compReturn() throws CodeGeneratorException {
        this.problematicReturn = false;
        if (inMain) {
            int outCount = 0;
            while (!semanticStack.peek().equals(delimiter)) {
                outCount += 1;
                Descriptor desc = activityRecord.getSymbolDecriptor((String) semanticStack.pop());
                if (!(desc instanceof PrimitiveVariableDescriptor)) {
                    throw new IncompatibleReturnException();
                }
                PrimitiveVariableType pvt = ((PrimitiveVariableDescriptor) desc).getPvt();
                if (!pvt.equals(Int)) {
                    throw new IncompatibleReturnException();
                }
            }
            //pop delimiter
            semanticStack.pop();
            if (outCount != 1) {
                throw new IncompatibleReturnException();
            }
            code.add(VMCodeLine.jump(codeEndLineImmideiate));
        } else {
            ArrayList<String> returnSyms = new ArrayList<>();
            while (!semanticStack.peek().equals(delimiter)) {
                returnSyms.add(0, (String) semanticStack.pop());
            }
            //pop delimiter
            semanticStack.pop();
            FunctionDescriptor fd = lastFunction;
            if (returnSyms.size() != fd.getOutputCount()) {
                throw new IncompatibleReturnException();
            }

            Operand outputDir = getTempOperand(Int, LocalDirect);
            Operand outputInDir = new Operand(LocalIndirect, Int, outputDir.getValue());
            code.add(VMCodeLine.assign(fd.getOutputPlace(), outputDir));
            for (int i = 0; i < returnSyms.size(); ++i) {
                VariableDescriptor vd = (VariableDescriptor) activityRecord.getSymbolDecriptor(returnSyms.get(i));
                if (fd.getOutputDescriptors().get(i).getVariableType().equals(Primitive)) {
                    if (!vd.getVariableType().equals(Primitive)) {
                        throw new IncompatibleVariableTypeException();
                    }
                    PrimitiveVariableType pvtFuncOut =
                            ((PrimitiveVariableDescriptor) fd.getOutputDescriptors().get(i)).getPvt();
                    PrimitiveVariableType pvtID = ((PrimitiveVariableDescriptor) vd).getPvt();
                    Operand casted = castCode(new Operand(LocalDirect, pvtID, vd.getAddress()), pvtFuncOut);
                    code.add(VMCodeLine.assign(casted,
                            new Operand(LocalIndirect, pvtFuncOut, outputDir.getValue())));
                } else {
                    if (fd.getOutputDescriptors().get(i).getVariableType().equals(Structure)) {
                        if (!vd.getVariableType().equals(Structure)) {
                            throw new IncompatibleVariableTypeException();
                        }
                        if (((StructureVariableDescriptor) fd.getOutputDescriptors().get(i)).getDataType() !=
                                ((StructureVariableDescriptor) vd).getDataType()) {
                            throw new IncompatibleVariableTypeException();
                        }
                    }

                    if (fd.getOutputDescriptors().get(i).getVariableType().equals(Array)) {
                        if (!vd.getVariableType().equals(Array)) {
                            throw new IncompatibleVariableTypeException();
                        }

                        ArrayVariableDescriptor funcOutDesc = (ArrayVariableDescriptor) fd.getOutputDescriptors().get(i);
                        ArrayVariableDescriptor givenOutDesc = (ArrayVariableDescriptor) vd;
                        if (funcOutDesc instanceof ArrayOfStringDescriptor) {
                            if (!(givenOutDesc instanceof ArrayOfStringDescriptor)) {
                                throw new IncompatibleVariableTypeException();
                            }
                        } else if (funcOutDesc instanceof ArrayOfStructureVariableDescriptor) {
                            if (!(givenOutDesc instanceof ArrayOfStructureVariableDescriptor)) {
                                throw new IncompatibleVariableTypeException();
                            }
                        } else if (funcOutDesc instanceof ArrayOfPrimitiveDescriptor) {
                            if (!(givenOutDesc instanceof ArrayOfPrimitiveDescriptor)) {
                                throw new IncompatibleVariableTypeException();
                            }

                            PrimitiveVariableType pvtFuncOut = ((ArrayOfPrimitiveDescriptor) funcOutDesc).getPrimitiveVariableType();
                            PrimitiveVariableType pvtID = ((ArrayOfPrimitiveDescriptor) givenOutDesc).getPrimitiveVariableType();
                            if (!pvtFuncOut.equals(pvtID)) {
                                throw new IncompatibleVariableTypeException();
                            }
                        }
                    }

                    if (fd.getOutputDescriptors().get(i).getVariableType().equals(String)) {
                        if (!vd.getVariableType().equals(String)) {
                            throw new IncompatibleVariableTypeException();
                        }
                    }

                    code.add(VMCodeLine.assign(new Operand(LocalDirect, Int, vd.getAddress()), outputInDir));
                }
                code.add(VMCodeLine.add(immideiateOne, outputDir, outputDir));
            }
            code.add(VMCodeLine.jump(fd.getReturnAdr()));
        }
    }

    private void checkRet() throws IncompatibleReturnException {
        if (this.problematicReturn) {
            throw new IncompatibleReturnException();
        }
    }

    private void release() throws NonExistentSymbolException, IncompatibleVariableTypeException, DuplicateSymbolAddException {
        String idSym = scanner.matchedText();
        Descriptor desc = activityRecord.getSymbolDecriptor(idSym);
        if (!(desc instanceof VariableDescriptor)) {
            throw new IncompatibleVariableTypeException();
        }
        VariableDescriptor vd = (VariableDescriptor) desc;
        if (vd instanceof ArrayVariableDescriptor) {
            ArrayVariableDescriptor avd = (ArrayVariableDescriptor) vd;
            Operand pointerToFirst = new Operand(activityRecord.getAddressingMod(idSym), Int, avd.getAddress());
            Operand pointer = getTempOperand(Int, LocalDirect);
            Operand pointerIndir = new Operand(LocalIndirect, Int, pointer.getValue());
            Operand dimCount = getTempOperand(Int, LocalDirect);
            Operand dimCountBackup = getTempOperand(Int, LocalDirect);
            Operand size = getTempOperand(Int, LocalDirect);
            Operand temp = getTempOperand(Int, LocalDirect);
            Operand cdn = getTempOperand(Bool, LocalDirect);
            code.add(VMCodeLine.assign(pointerToFirst, pointer));
            code.add(VMCodeLine.assign(pointerIndir, dimCount));
            code.add(VMCodeLine.assign(dimCount, dimCountBackup));
            code.add(VMCodeLine.assign(immideiateOne, size));

            Operand jumpBackAdr = getImmediateOperand(getNextLineAddress());
            code.add(VMCodeLine.add(pointer, immideiateOne, pointer));
            code.add(VMCodeLine.assign(pointerIndir, temp));
            code.add(VMCodeLine.multiply(size, temp, size));
            code.add(VMCodeLine.subtract(dimCount, immideiateOne, dimCount));
            code.add(VMCodeLine.lessThanOrEqual(dimCount, immideiateZero, cdn));
            code.add(VMCodeLine.jumpZero(cdn, jumpBackAdr));
            if (avd instanceof ArrayOfPrimitiveDescriptor &&
                    ((ArrayOfPrimitiveDescriptor) avd).getPrimitiveVariableType().equals(LongInt)) {
                code.add(VMCodeLine.multiply(size, getImmediateOperand(2), size));
            }
            code.add(VMCodeLine.add(dimCountBackup, size, size));
            code.add(VMCodeLine.add(immideiateOne, size, size));
            code.add(VMCodeLine.freeMemory(pointerToFirst, size));
            code.add(VMCodeLine.assign(immideiateMinusOne, pointerToFirst));

        } else if (vd instanceof StructureVariableDescriptor) {
            Operand structureIdOperand = new Operand(LocalDirect, Int, vd.getAddress());
            StructureVariableDescriptor svd = (StructureVariableDescriptor) vd;
            code.add(VMCodeLine.freeMemory(structureIdOperand, getImmediateOperand(svd.getDataType().getDataSize())));
            code.add(VMCodeLine.assign(immideiateMinusOne, structureIdOperand));
        } else if (vd instanceof StringVariableDescriptor) {
            Operand stringIdOperand = new Operand(LocalDirect, Int, vd.getAddress());
            semanticStack.push(stringIdOperand);
            strlen_push();
            Operand len = (Operand) semanticStack.pop();
            code.add(VMCodeLine.add(immideiateOne, len, len));
            code.add(VMCodeLine.freeMemory(stringIdOperand, len));
            code.add(VMCodeLine.assign(immideiateMinusOne, stringIdOperand));
        }
    }

    private void write() throws IncompatibleVariableTypeException, DuplicateSymbolAddException {
        Operand pure_op = (Operand) semanticStack.pop();
        if (semanticStack.peek().equals(delimiter) || semanticStack.peek() instanceof StructureVariableDescriptor ||
                semanticStack.peek() instanceof ArrayVariableDescriptor) {
            throw new IncompatibleVariableTypeException();
        }
        Operand op = getTempOperand(pure_op.getPrimitiveVariableType(), LocalDirect);
        code.add(VMCodeLine.assign(pure_op, op));
        if (pure_op.isStringPointer()) {
            op.setStringPointer(true);
        }

        if (op.isStringPointer()) {
            Operand pointerDir = getTempOperand(Int, LocalDirect);
            Operand pointerInDir = new Operand(LocalIndirect, Char, pointerDir.getValue());
            Operand cdn = getTempOperand(Bool, LocalDirect);
            Operand jumpAdr = getImmediateOperand(-1);
            code.add(VMCodeLine.assign(op, pointerDir));
            Operand jumpBack = getImmediateOperand(getNextLineAddress());
            code.add(VMCodeLine.notEquals(new Operand(LocalIndirect, Int, pointerDir.getValue()),
                    immideiateNullCharAsInt, cdn));
            code.add(VMCodeLine.jumpZero(cdn, jumpAdr));
            code.add(VMCodeLine.writeText(pointerInDir));
            code.add(VMCodeLine.add(immideiateOne, pointerDir, pointerDir));
            code.add(VMCodeLine.jump(jumpBack));
            jumpAdr.setValue(getNextLineAddress());
        } else {
            switch (op.getPrimitiveVariableType()) {
                case Int:
                    code.add(VMCodeLine.writeInteger(op));
                    break;
                case LongInt:
                    //Todo: maybe add write for long but it is hard
                    break;
                case Char:
                    code.add(VMCodeLine.writeText(op));
                    break;
                case Bool:
                    code.add(VMCodeLine.writeInteger(new Operand(op.getAddressingMode(), Int, op.getValue())));
                    break;
                case Real:
                    code.add(VMCodeLine.writeFloat(op));
                    break;
            }
        }
    }

    private void makeJ_Push() throws CodeGeneratorException {
        Operand op = castCode((Operand) semanticStack.pop(), Bool);
        Operand jumpAdr = getImmediateOperand(-1);
        code.add(VMCodeLine.jumpZero(op, jumpAdr));
        semanticStack.push(jumpAdr);
    }

    private void popRecord_NOP_compJ() throws DuplicateSymbolAddException, NonExistentLabelException, GlobalRecordRemoveException, ProblematicCodeException {
        activityRecord.popRecord();
        Operand jumpAdr = (Operand) semanticStack.pop();
        if (!jumpAdr.getAddressingMode().equals(Immidiate)) {
            throw new ProblematicCodeException();
        }
        code.add(VMCodeLine.NOP());
        jumpAdr.setValue(getNextLineAddress());

    }

    private void makeJ_push_addRecord() {
        Operand jumpAdr = getImmediateOperand(-1);
        code.set(code.size() - 1, VMCodeLine.jump(jumpAdr));
        semanticStack.push(jumpAdr);
        activityRecord.addNewRecord();
    }

    private void compJ_popRecord() throws DuplicateSymbolAddException, NonExistentLabelException, GlobalRecordRemoveException, ProblematicCodeException {
        activityRecord.popRecord();
        Operand jumpAdr = (Operand) semanticStack.pop();
        if (!jumpAdr.getAddressingMode().equals(Immidiate)) {
            throw new ProblematicCodeException();
        }
        jumpAdr.setValue(getNextLineAddress());
    }

    private void makeJ() throws CodeGeneratorException {
        Operand cdn = castCode((Operand) semanticStack.pop(), Bool);
        code.add(VMCodeLine.jumpZero(cdn, breakStack.peek()));

    }

    private void initContBreak_push() {
        Operand continueDest = getImmediateOperand(getNextLineAddress());
        continueStack.push(continueDest);
        Operand breakDest = getImmediateOperand(-1);
        breakStack.push(breakDest);
    }

    private void popRecord_compContBreakPop() throws DuplicateSymbolAddException, NonExistentLabelException, GlobalRecordRemoveException {
        activityRecord.popRecord();
        Operand breakDest = breakStack.pop();
        code.add(VMCodeLine.jump(continueStack.pop()));
        breakDest.setValue(getNextLineAddress());
    }

    private void jumpBreak() {
        if (breakStack.size() < 1) {
            new IllegalBreakException();
        }
        code.add(VMCodeLine.jump(breakStack.peek()));
    }

    private void jumpContinue() {
        if (continueStack.size() < 1) {
            new IllegalContinueException();
        }
        code.add(VMCodeLine.jump(continueStack.peek()));
    }

    private void addRecord_initContBrBJ_push() {
        activityRecord.addNewRecord();
        Operand continueDest = getImmediateOperand(-1);
        continueStack.push(continueDest);
        Operand breakDest = getImmediateOperand(-1);
        breakStack.push(breakDest);
        semanticStack.push(getImmediateOperand(getNextLineAddress()));
    }

    private void compCont() {
        continueStack.peek().setValue(getNextLineAddress());
    }

    private void makeDoWhileFinalJump() throws CodeGeneratorException {
        Operand cdn = castCode((Operand) semanticStack.pop(), Bool);
        code.add(VMCodeLine.logicalNot(cdn, cdn));
        code.add(VMCodeLine.jumpZero(cdn, (Operand) semanticStack.pop()));
    }

    private void compBr_popBrCont() {
        breakStack.peek().setValue(getNextLineAddress());
        breakStack.pop();
        continueStack.pop();
    }


    private void read() throws IncompatibleVariableTypeException, DuplicateSymbolAddException {
        Operand op = (Operand) semanticStack.pop();
        if (semanticStack.peek() instanceof StructureVariableDescriptor ||
                semanticStack.peek() instanceof ArrayVariableDescriptor) {
            throw new IncompatibleVariableTypeException();
        }
        if (op.isStringPointer()) {
            code.add(VMCodeLine.getMemory(getImmediateOperand(maxStringInputSize), op));
            Operand charPointerInDir = getTempOperand(Char, LocalIndirect);
            Operand charPointerInDirInt = new Operand(LocalIndirect, Int, charPointerInDir.getValue());
            Operand charPointerDir = new Operand(LocalDirect, Int, charPointerInDir.getValue());
            Operand cdn = getTempOperand(Bool, LocalDirect);
            Operand rest = getTempOperand(Int, LocalDirect);
            code.add(VMCodeLine.assign(getImmediateOperand(maxStringInputSize), rest));
            code.add(VMCodeLine.assign(op, charPointerDir));
            Operand jumpAdr = getImmediateOperand(getNextLineAddress());
            code.add(VMCodeLine.readText(charPointerInDir));
            code.add(VMCodeLine.equals(charPointerInDirInt, immideiateEndLineCharAsInt, cdn));
            code.add(VMCodeLine.add(charPointerDir, immideiateOne, charPointerDir));
            code.add(VMCodeLine.subtract(rest, immideiateOne, rest));
            code.add(VMCodeLine.jumpZero(cdn, jumpAdr));
            code.add(VMCodeLine.subtract(charPointerDir, immideiateOne, charPointerDir));
            code.add(VMCodeLine.assign(immideiateNullCharAsInt, charPointerInDirInt));
            code.add(VMCodeLine.add(charPointerDir, immideiateOne, charPointerDir));
            code.add(VMCodeLine.subtract(rest, immideiateOne, rest));
            code.add(VMCodeLine.freeMemory(charPointerDir, rest));

        } else {
            switch (op.getPrimitiveVariableType()) {
                case Int:
                    code.add(VMCodeLine.readInteger(op));
                    break;
                case Char:
                    code.add(VMCodeLine.readText(op));
                    break;
                case Bool:
                    Operand inputAsInt = getTempOperand(Int, LocalDirect);
                    code.add(VMCodeLine.readInteger(inputAsInt));
                    Operand temp = castToBooleanCode(inputAsInt);
                    code.add(VMCodeLine.assign(temp, op));
                    break;
                case Real:
                    code.add(VMCodeLine.readInteger(op));
                    break;
                case LongInt:
                    //TODO: maybe add read for long int but very hard
                    break;
            }
        }
    }

    private void initEndAdr_push() {
        Operand endAdr = getImmediateOperand(-1);
        Operand tableAdr = getImmediateOperand(-1);
        code.add(VMCodeLine.jump(tableAdr));
        semanticStack.push(tableAdr);
        semanticStack.push(delimiter);
        semanticStack.push(endAdr);
    }

    private void pushBlockAdrOp_pushConsUnderEndAdr() throws IllegalTypeInSwitchCaseException {
        Operand blockStartAdr = getImmediateOperand(getNextLineAddress());
        if (!(scanner.tokenValue() instanceof Integer)) {
            throw new IllegalTypeInSwitchCaseException();
        }
        Integer constant = (Integer) scanner.tokenValue();
        Object endAdr = semanticStack.pop();
        semanticStack.push(blockStartAdr);
        semanticStack.push(constant);
        semanticStack.push(endAdr);
    }

    private void popRecord_jumpToEnd() throws DuplicateSymbolAddException, NonExistentLabelException, GlobalRecordRemoveException {
        activityRecord.popRecord();
        code.add(VMCodeLine.jump((Operand) semanticStack.peek()));
    }

    private void compSwitchCase() throws DuplicateSymbolAddException {
        ArrayList<Integer> constants = new ArrayList<>();
        ArrayList<Operand> blockAdrOperands = new ArrayList<>();
        Operand endAdr = (Operand) semanticStack.pop();
        while (!semanticStack.peek().equals(delimiter)) {
            constants.add((Integer) semanticStack.pop());
            blockAdrOperands.add((Operand) semanticStack.pop());
        }
        semanticStack.pop(); //pops delimiter
        Operand tableAdr = (Operand) semanticStack.pop();
        semanticStack.pop(); //pops delimiter
        Operand pure_expr = (Operand) semanticStack.pop();
        Operand expr = getTempOperand(Int, LocalDirect);
        int max = Collections.max(constants);
        int min = Collections.min(constants);
        Operand maxConstOp = getImmediateOperand(max);
        Operand minConstOp = getImmediateOperand(min);
        Operand isLessThanMin = getTempOperand(Bool, LocalDirect);
        Operand isBiggerThanMax = getTempOperand(Bool, LocalDirect);
        Operand isBad = getTempOperand(Bool, LocalDirect);
        Operand isGood = getTempOperand(Bool, LocalDirect);
        tableAdr.setValue(getNextLineAddress());
        code.add(VMCodeLine.assign(pure_expr, expr));
        code.add(VMCodeLine.lessThan(expr, minConstOp, isLessThanMin));
        code.add(VMCodeLine.greaterThan(expr, maxConstOp, isBiggerThanMax));
        code.add(VMCodeLine.logicalOr(isLessThanMin, isBiggerThanMax, isBad));
        code.add(VMCodeLine.logicalNot(isBad, isGood));
        code.add(VMCodeLine.jumpZero(isGood, endAdr));
        code.add(VMCodeLine.subtract(expr, minConstOp, expr));
        code.add(VMCodeLine.add(expr, getImmediateOperand(getNextLineAddress() + 2), expr));
        code.add(VMCodeLine.jump(expr));
        for (int i = min; i <= max; ++i) {
            int index = constants.indexOf(i);
            if (index == -1) {
                code.add(VMCodeLine.jump(endAdr));
            } else {
                code.add(VMCodeLine.jump(blockAdrOperands.get(index)));
            }
        }
        endAdr.setValue(getNextLineAddress());
    }
}
