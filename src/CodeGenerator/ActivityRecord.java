package CodeGenerator;

import CodeGenerator.Descriptors.Descriptor;
import CodeGenerator.Descriptors.LabelDescriptor;
import CodeGenerator.Descriptors.Type;
import Exceptions.DuplicateSymbolAddException;
import Exceptions.GlobalRecordRemoveException;
import Exceptions.NonExistentLabelException;
import Exceptions.NonExistentSymbolException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rasa on 2/1/17.
 */
public class ActivityRecord {
    private ArrayList<HashMap<String, Descriptor>> record;
    private int globalDataAddressCounter, localDataAddressCounter;

    public ActivityRecord() {
        record = new ArrayList<>();
        record.add(new HashMap<>()); //for global symbol table
        globalDataAddressCounter = 0;
        localDataAddressCounter = 0;
    }


    public void addNewRecord() {
        record.add(new HashMap<>());
    }

    public void popRecord() throws GlobalRecordRemoveException, DuplicateSymbolAddException, NonExistentLabelException {
        if (record.size() == 1) {
            throw new GlobalRecordRemoveException();
        }

        HashMap<String, Descriptor> topTable = record.remove(record.size() - 1);

        if (atLocalScope()) {

            /**  labels are not bound to scopes
             *  so here adds all the labels from gotos
             *  or label definitions back to top record
             *  unless of course we are back at global scope
             */

            for (String symbol : topTable.keySet()) {
                Descriptor desc = topTable.get(symbol);
                if (desc.getType().equals(Type.Label)) {
                    this.addSymbol(symbol, desc);
                }
            }
        } else {
            /**
             * When exiting out of function
             * if label is still undefined
             * we have got a problem
             * the same is not true for blocks
             */
            for (String symbol : topTable.keySet()) {
                Descriptor desc = topTable.get(symbol);
                if (desc.getType().equals(Type.Label)) {
                    LabelDescriptor ld = (LabelDescriptor) desc;
                    if (!ld.isDefined()) {
                        throw new NonExistentLabelException();
                    }
                }
            }
            /**
             * out of local scope so resetting
             * local data size
             */
            localDataAddressCounter = 0;
        }

    }

    public int getDepth() {
        return record.size() - 1;
    }

    /**
     * can be used to update descriptor of symbol
     *
     * @param symbol
     * @return descriptor object of symbol
     * @throws NonExistentSymbolException
     */
    public Descriptor getSymbolDecriptor(String symbol) throws NonExistentSymbolException {
        int i = record.size() - 1;
        Descriptor desc;

        while (i >= 0) {
            HashMap<String, Descriptor> symbolTable = record.get(i);
            desc = symbolTable.get(symbol);
            if (null != desc) {
                return desc;
            }
            i -= 1;
        }

        throw new NonExistentSymbolException();
    }


    public void addSymbol(String symbol, Descriptor desc) throws DuplicateSymbolAddException {
        HashMap<String, Descriptor> symbolTable = record.get(record.size() - 1);
        if (symbolTable.get(symbol) != null) {
            throw new DuplicateSymbolAddException();
        } else {
            symbolTable.put(symbol, desc);
            if (atLocalScope()) {
                localDataAddressCounter += desc.getSize();
            } else {
                globalDataAddressCounter += desc.getSize();
            }
        }
    }


    public void addSymbolToGlobal(String symbol, Descriptor desc) throws DuplicateSymbolAddException {
        HashMap<String, Descriptor> globalSymTable = record.get(0);
        if (globalSymTable.get(symbol) != null) {
            throw new DuplicateSymbolAddException();
        } else {
            globalSymTable.put(symbol, desc);
            globalDataAddressCounter += desc.getSize();
        }

    }

    public boolean atLocalScope() {
        return (this.record.size() > 1);
    }

    public boolean atGlobalScope() {
        return !this.atLocalScope();
    }


    /**
     * only works correctly if called at end of function
     * and function only. not block not environment ...
     *
     * @return current local data size
     */
    public int getLocalDataAddressCounter() {
        return localDataAddressCounter;
    }

    public int getGlobalDataAddressCounter() {
        return globalDataAddressCounter;
    }

    public void updateAddressCounter(int add) {
        if (atLocalScope()) localDataAddressCounter += add;
        if (atGlobalScope()) globalDataAddressCounter += add;
    }

    public int getDataAdr() {
        if (atGlobalScope()) return globalDataAddressCounter;
        else return localDataAddressCounter;
    }

    private HashMap<String, Descriptor> getTopRecord() {
        return this.record.get(record.size() - 1);
    }

    public boolean isDefinedInGlobal(String symbol) {
        HashMap<String, Descriptor> globalSymTable = record.get(0);
        return (globalSymTable.get(symbol) != null);
    }

    public AddressingMode getAddressingMod(String symbol) throws NonExistentSymbolException {
        int i = record.size() - 1;
        Descriptor desc;

        while (i >= 0) {
            HashMap<String, Descriptor> symbolTable = record.get(i);
            desc = symbolTable.get(symbol);
            if (null != desc) {
                if (i > 0) {
                    return AddressingMode.LocalDirect;
                } else {
                    return AddressingMode.GlobalDirect;
                }
            }
            i -= 1;
        }

        throw new NonExistentSymbolException();

    }

    public boolean isOnlyInGlobal(String symbol) {
        for (int i = record.size() - 1; i > 0; --i) {
            HashMap<String, Descriptor> symbolTable = record.get(i);
            if (null != symbolTable.get(symbol)) {
                return false;
            }
        }
        return isDefinedInGlobal(symbol);

    }
}
