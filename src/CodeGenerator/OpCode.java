package CodeGenerator;

/**
 * Created by rasa on 2/1/17.
 */
public enum OpCode {
    Add("+", 3), Subtract("-", 3), Multiply("*", 3), Divide("/", 3),
    Mod("%", 3), LogicalAnd("&&", 3), LogicalOr("||", 3),
    BinaryAnd("&", 3), BinaryOr("|", 3), BinaryXor("^", 3),
    BinaryNot("~", 2), LessThan("<", 3), GreaterThan(">", 3),
    LogicalLeftShift("<<", 2), LogicalRightShift(">>", 2),
    LessThanEqual("<=", 3), GreaterThanEqual(">=", 3),
    EqualityCheck("==", 3), NotEqual("!=", 3), LogicalNot("!", 2),
    UnaryMinus("u-", 2), Assignment(":=", 2), JumpZero("jz", 2),
    Jump("jmp", 1), WriteInteger("wi", 1), WriteFloat("wf", 1),
    WriteText("wt", 1), ReadInteger("ri", 1), ReadFloat("rf", 1),
    ReadText("rt", 1), GetMemory("gmm", 2), FreeMemory("fmm", 2),
    PcGet(":=pc", 1), SpGet(":=sp", 1), IncSp("+sp", 1),
    DecSp("-sp", 1), AssignSp("sp:=", 1), GetOverFlow(":=v", 1);

    private final String codedForVm;
    private final int operandCount;

    OpCode(String symbol, int operandCount) {
        this.codedForVm = symbol;
        this.operandCount = operandCount;
    }

    @Override
    public String toString() {
        return this.codedForVm;
    }

    public int getOperandCount() {
        return operandCount;
    }

}
