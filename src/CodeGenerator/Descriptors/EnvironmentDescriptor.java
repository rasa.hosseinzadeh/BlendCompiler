package CodeGenerator.Descriptors;

import Exceptions.DuplicateVariableException;
import Exceptions.NonExistentVariableException;

import java.util.HashMap;

/**
 * Created by rasa on 2/1/17.
 */
public class EnvironmentDescriptor extends Descriptor {
    private int address;
    private int addressCounter;
    private HashMap<String, VariableDescriptor> symbolTable;

    public EnvironmentDescriptor(Integer address) {
        super(Type.Environment);
        this.address = address;
        this.addressCounter = 0;
        this.symbolTable = new HashMap<>();
    }


    /**
     * @param symbol name of symbol
     * @param desc   create a descriptor based on type(int, bool, struct, array)
     *               the address field is set again based on structure adddress counter
     *               inside this method so no need to set it
     * @throws DuplicateVariableException
     */
    public void addSymbol(String symbol, VariableDescriptor desc) throws DuplicateVariableException {
        if (symbolTable.containsKey(symbol)) {
            throw new DuplicateVariableException();
        }
        desc.setAddress(desc.getAddress());
        addressCounter += desc.getSize();
        symbolTable.put(symbol, desc);
    }


    public Descriptor getSymbolDescriptor(String symbol) throws NonExistentVariableException {
        if (!symbolTable.containsKey(symbol)) {
            throw new NonExistentVariableException();
        }
        return symbolTable.get(symbol);
    }

    public int getAddress() {
        return address;
    }

    public int getDataSize() {
        return addressCounter;
    }

    @Override
    public int getSize() {
        return this.getDataSize();
    }
}
