package CodeGenerator.Descriptors;

/**
 * Created by rasa on 1/30/17.
 */
public abstract class VariableDescriptor extends Descriptor {
    private VariableType variableType;
    private int address;

    public VariableDescriptor(Integer address, VariableType variableType) {
        super(Type.Variable);
        this.variableType = variableType;
        this.address = address;
    }

    @Override
    public abstract int getSize();

    public VariableType getVariableType() {
        return variableType;
    }

    public int getAddress() {
        return address;
    }

    public void setAddress(int address) {
        this.address = address;
    }
}
