package CodeGenerator.Descriptors;

/**
 * Created by rasa on 2/2/17.
 */
public class ArrayOfPrimitiveDescriptor extends ArrayVariableDescriptor {
    private PrimitiveVariableType primitiveVariableType;

    /***
     *  @param address the address of pointer of array
     */
    public ArrayOfPrimitiveDescriptor(Integer address, PrimitiveVariableType primitiveVariableTypeType) {
        super(address, VariableType.Primitive);
        this.primitiveVariableType = primitiveVariableTypeType;
    }

    public PrimitiveVariableType getPrimitiveVariableType() {
        return primitiveVariableType;
    }
}
