package CodeGenerator.Descriptors;

import java.util.ArrayList;

/**
 * Created by rasa on 1/31/17.
 */
public class LabelDescriptor extends Descriptor {
    private int lineNum;
    private ArrayList<Integer> gotoLines;

    /**
     * Constructs a label descriptor with no additional data
     * this is because of handling general case
     * if goto comes before label we can call this
     * if label comes first
     * we call this and then call setLineNum
     */
    public LabelDescriptor() {
        super(Type.Label);
        this.gotoLines = new ArrayList<>();
        this.lineNum = -1;
    }

    public int getLineNum() {
        return lineNum;
    }

    public void setLineNum(int lineNum) {
        this.lineNum = lineNum;
    }

    /**
     * @param gotoLine the line with goto to this label
     *                 always call this at goto
     *                 here we traverse all the getGotoLines
     *                 and update all the instructions
     *                 to point to this lable
     *                 unless the label is -1
     *                 which is an error
     */
    public void addGoto(int gotoLine) {
        gotoLines.add(gotoLine);
    }

    public ArrayList<Integer> getGotoLines() {
        return this.gotoLines;
    }

    public boolean isDefined() {
        return this.lineNum != -1;
    }
}
