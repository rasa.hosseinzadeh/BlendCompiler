package CodeGenerator.Descriptors;

/**
 * Created by rasa on 1/30/17.
 */
public abstract class ArrayVariableDescriptor extends VariableDescriptor {

    private VariableType dataType;

    /***
     *
     * @param address the address of pointer of array
     * @param dataType type of data that array holds
     *                 this cannot be array itself
     *                 though other types are permitted
     */
    public ArrayVariableDescriptor(Integer address, VariableType dataType) {
        super(address, VariableType.Array);
        this.dataType = dataType;
    }

    @Override
    public int getSize() {
        return pointerSize;
    }

    public VariableType getDataType() {
        return dataType;
    }
}
