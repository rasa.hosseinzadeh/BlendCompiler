package CodeGenerator.Descriptors;

/**
 * Created by rasa on 1/30/17.
 */
public class StructureVariableDescriptor extends VariableDescriptor {
    private StructureDescriptor dataType;

    public StructureVariableDescriptor(Integer address, StructureDescriptor dataType) {
        super(address, VariableType.Structure);
        this.dataType = dataType;
    }

    @Override
    public int getSize() {
        return pointerSize;
    }

    public StructureDescriptor getDataType() {
        return dataType;
    }
}
