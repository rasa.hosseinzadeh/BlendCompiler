package CodeGenerator.Descriptors;

/**
 * Created by rasa on 1/30/17.
 */
public enum VariableType {
    Array, Structure, Primitive, String,
}
