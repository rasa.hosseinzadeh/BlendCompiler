package CodeGenerator.Descriptors;

/**
 * Created by rasa on 1/30/17.
 */

public abstract class Descriptor {

    public static int wordSize = 1;
    public static int pointerSize = wordSize; //in bytes
    private Type type;

    public Descriptor(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }

    /**
     * this is overridden in types with actual data
     * on memory but for other types like StructureDescriptor
     * is zero
     *
     * @return
     */
    public int getSize() {
        return 0;
    }

}
