package CodeGenerator.Descriptors;

import Exceptions.DuplicateVariableException;
import Exceptions.NonExistentVariableException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by rasa on 1/30/17.
 */
public class StructureDescriptor extends Descriptor {


    private int addressCounter;
    private HashMap<String, VariableDescriptor> symbolTable;
    private ArrayList<VariableDescriptor> order;
    public StructureDescriptor() {
        super(Type.Structure);
        symbolTable = new HashMap<>();
        order = new ArrayList<>();
        addressCounter = 0;
    }

    /**
     * @param symbol name of symbol
     * @param desc   create a descriptor based on type(int, bool, struct, array)
     *               the address field is set again based on structure adddress counter
     *               inside method so no need to set it
     *               inside method
     * @throws DuplicateVariableException
     */
    public void addSymbol(String symbol, VariableDescriptor desc) throws DuplicateVariableException {
        if (symbolTable.containsKey(symbol)) {
            throw new DuplicateVariableException();
        }
        desc.setAddress(addressCounter);
        addressCounter += desc.getSize();
        symbolTable.put(symbol, desc);
        order.add(desc);
    }

    public VariableDescriptor getSymbolDescriptor(String symbol) throws NonExistentVariableException {
        if (!symbolTable.containsKey(symbol)) {
            throw new NonExistentVariableException();
        }
        return symbolTable.get(symbol);
    }

    public int getDataSize() {
        return addressCounter;
    }

    public VariableDescriptor getDescriptorByOrder(int index) {
        return order.get(index);
    }

    public int getDataCount() {
        return order.size();
    }
}
