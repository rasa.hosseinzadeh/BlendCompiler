package CodeGenerator.Descriptors;

/**
 * Created by rasa on 1/30/17.
 */
public enum PrimitiveVariableType {
    Int, LongInt, Char, Bool, Real;

    public static PrimitiveVariableType getCastType(PrimitiveVariableType pvt1, PrimitiveVariableType pvt2) {
        if (pvt1.equals(pvt2)) return pvt1;
        if (pvt1.equals(Real) || pvt2.equals(Real)) return Real;
        if (pvt1.equals(LongInt) || pvt2.equals(LongInt)) return LongInt;
        if (pvt1.equals(Int) || pvt2.equals(Int)) return Int;
        if (pvt1.equals(Char) || pvt2.equals(Char)) return Char;
        return Bool;
    }

    /**
     * Vm is supporting string as a primitive type
     * thpuogh its not working so i didn't add anything for string yet
     * this class might change
     * @return Size of this primitive in bytes
     */
    public int getSize() {
        switch (this) {
            case Int:
                return 1;
            case Char:
                return 1;
            case Bool:
                return 1;
            case Real:
                return 1;
            case LongInt:
                return 2;

        }

        return 0;

    }

    /**
     * @return the prefix in Vm for this type
     * stupud vm doesnt support long so
     * even though long is a primitive variable type
     * dont send long to this
     * long must be implemented by us using int
     */
    public String getPrefix() {
        switch (this) {
            case Int:
                return "i_";
            case Char:
                return "c_";
            case Bool:
                return "b_";
            case Real:
                return "f_";


        }

        return null;
    }

}
