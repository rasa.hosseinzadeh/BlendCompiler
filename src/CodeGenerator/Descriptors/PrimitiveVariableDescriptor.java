package CodeGenerator.Descriptors;

/**
 * Created by rasa on 1/30/17.
 */
public class PrimitiveVariableDescriptor extends VariableDescriptor {

    private PrimitiveVariableType pvt;
    public PrimitiveVariableDescriptor(Integer address, PrimitiveVariableType pvt) {
        super(address, VariableType.Primitive);
        this.pvt = pvt;
    }

    public PrimitiveVariableType getPvt() {
        return pvt;
    }

    @Override
    public int getSize() {
        return pvt.getSize();
    }
}
