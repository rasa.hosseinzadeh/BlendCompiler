package CodeGenerator.Descriptors;

/**
 * Created by rasa on 1/30/17.
 */
public class StringVariableDescriptor extends VariableDescriptor {
    public StringVariableDescriptor(Integer address) {
        super(address, VariableType.String);
    }

    @Override
    public int getSize() {
        return pointerSize;
    }
}
