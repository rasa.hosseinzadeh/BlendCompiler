package CodeGenerator.Descriptors;

import CodeGenerator.Operand;

import java.util.ArrayList;

import static CodeGenerator.AddressingMode.Immidiate;
import static CodeGenerator.AddressingMode.LocalDirect;
import static CodeGenerator.Descriptors.PrimitiveVariableType.Int;

/**
 * Created by rasa on 2/1/17.
 */

/**
 * Get meteor in caller
 * Stack pointer changes in function
 * Order is from top to bottom in stack:
 * 1. Previous stack pointer
 * 2. Output data adr
 * 3. Input data adr
 * 4. Return adr
 * ---> previous stack pointer, points to here.
 */
public class FunctionDescriptor extends Descriptor {
    public static int metaDataWordCount = 4;
    private int startAddress, dataSize;
    private ArrayList<String> inputSymbols;
    private ArrayList<VariableDescriptor> inputDescriptors;
    private ArrayList<VariableDescriptor> outputDescriptors;
    private Operand prevStackPointer, inputPlace, outputPlace, returnAdr, dataSizeOperand;

    public FunctionDescriptor() {
        super(Type.Function);
        this.inputDescriptors = new ArrayList<>();
        this.inputSymbols = new ArrayList<>();
        this.outputDescriptors = new ArrayList<>();
        this.prevStackPointer = new Operand(LocalDirect, Int, -1);
        this.inputPlace = new Operand(LocalDirect, Int, -1);
        this.outputPlace = new Operand(LocalDirect, Int, -1);
        this.returnAdr = new Operand(LocalDirect, Int, -1);
        this.dataSizeOperand = new Operand(Immidiate, Int, -1);

    }


    public static int getReturnAddressAddressRelativeToCallerStackPointer() {
        return -pointerSize;
    }

    public static int getInputDataAddressAddressRelativeToCallerStackPointer() {
        return -pointerSize - pointerSize;
    }

    public static int getOutputAddressAddressRelativeToCallerStackPointer() {
        return -pointerSize - pointerSize - pointerSize;
    }

    public static int getPreviousStackPointerAddressRelativeToCallerStackPointer() {
        return -pointerSize - pointerSize - pointerSize - pointerSize;
    }

    public int getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(int startAddress) {
        this.startAddress = startAddress;
    }

    /**
     * sum of all data plus 4 words at the end
     * for return addresss
     * for input data address
     * for output data address
     * for previous stack pointer
     * in that order
     */
    public int getDataSize() {
        return dataSize + metaDataWordCount * pointerSize;
    }

    public void setDataSize(int dataSize) {
        this.dataSize = dataSize;
        prevStackPointer.setValue(getDataSize() + getPreviousStackPointerAddressRelativeToCallerStackPointer());
        outputPlace.setValue(getDataSize() + getOutputAddressAddressRelativeToCallerStackPointer());
        inputPlace.setValue(getDataSize() + getInputDataAddressAddressRelativeToCallerStackPointer());
        returnAdr.setValue(getDataSize() + getReturnAddressAddressRelativeToCallerStackPointer());
        this.dataSizeOperand.setValue(dataSize + metaDataWordCount * pointerSize);
    }

    public Operand getDataSizeImmidiateOperand() {
        return dataSizeOperand;
    }

    public void addInputSymbol(String symbol, VariableDescriptor desc) {
        inputDescriptors.add(desc);
        inputSymbols.add(symbol);
    }

    public Integer getInputCount() {
        return inputDescriptors.size();
    }
    public Integer getInputSize() {
        int size = 0;
        for (VariableDescriptor vd : inputDescriptors) {
            size += vd.getSize();
        }
        return size;
    }

    public void addOutputSymbol(VariableDescriptor variableDescriptor) {
        outputDescriptors.add(variableDescriptor);
    }

    public Integer getOutputCount() {
        return outputDescriptors.size();
    }

    public Integer getOutputSize() {
        int size = 0;
        for (VariableDescriptor vd : outputDescriptors) {
            size += vd.getSize();
        }
        return size;
    }

    public ArrayList<VariableDescriptor> getOutputDescriptors() {
        return outputDescriptors;
    }

    public ArrayList<VariableDescriptor> getInputDescriptors() {
        return inputDescriptors;
    }

    public Operand getPrevStackPointer() {
        return prevStackPointer;
    }

    public Operand getInputPlace() {
        return inputPlace;
    }

    public Operand getOutputPlace() {
        return outputPlace;
    }

    public Operand getReturnAdr() {
        return returnAdr;
    }
}
