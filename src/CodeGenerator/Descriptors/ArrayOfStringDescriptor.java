package CodeGenerator.Descriptors;

/**
 * Created by rasa on 2/2/17.
 */
public class ArrayOfStringDescriptor extends ArrayVariableDescriptor {
    /***
     *  @param address the address of pointer of array
     */
    public ArrayOfStringDescriptor(Integer address) {
        super(address, VariableType.String);
    }
}
