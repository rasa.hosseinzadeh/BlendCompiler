package CodeGenerator.Descriptors;

import Exceptions.ConstantTypeException;

/**
 * Created by rasa on 1/30/17.
 */
public class ConstantDescriptor extends Descriptor {
    private Object value;
    private PrimitiveVariableType primitiveVariableType;

    public ConstantDescriptor(Object val, PrimitiveVariableType primitiveVariableType) throws ConstantTypeException {
        super(Type.Constant);
        value = val;
        this.primitiveVariableType = primitiveVariableType;

        switch (primitiveVariableType) {
            case Int:
                if (!(value instanceof Integer))
                    throw new ConstantTypeException();
                break;
            case LongInt:
                if (!(value instanceof Long))
                    throw new ConstantTypeException();
                break;
            case Char:
                if (!(value instanceof Character))
                    throw new ConstantTypeException();
                break;
            case Bool:
                if (!(value instanceof Boolean))
                    throw new ConstantTypeException();
                break;
            case Real:
                if (!(value instanceof Float))
                    throw new ConstantTypeException();
                break;
        }
    }

    public Object getValue() {
        return value;
    }

    public PrimitiveVariableType getPrimitiveVariableType() {
        return primitiveVariableType;
    }
}
