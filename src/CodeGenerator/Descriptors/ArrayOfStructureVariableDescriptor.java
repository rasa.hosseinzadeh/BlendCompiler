package CodeGenerator.Descriptors;

/**
 * Created by rasa on 2/2/17.
 */
public class ArrayOfStructureVariableDescriptor extends ArrayVariableDescriptor {
    /***
     *  @param address the address of pointer of array
     *  @param structureDescriptor pointer to structure desc
     */
    private StructureDescriptor structureDescriptor;

    public ArrayOfStructureVariableDescriptor(Integer address, StructureDescriptor structureDescriptor) {
        super(address, VariableType.Structure);
        this.structureDescriptor = structureDescriptor;
    }

    public StructureDescriptor getStructureDescriptor() {
        return structureDescriptor;
    }
}
