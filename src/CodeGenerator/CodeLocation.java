package CodeGenerator;

/**
 * Created by rasa on 2/2/17.
 */
public enum CodeLocation {
    Function, Global, Structure, Environment
}
