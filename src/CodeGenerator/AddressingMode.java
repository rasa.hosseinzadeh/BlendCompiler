package CodeGenerator;


import Exceptions.CodeGeneratorException;

/**
 * Created by rasa on 1/31/17.
 */
public enum AddressingMode {
    GlobalDirect, GlobalIndirect, LocalDirect, LocalIndirect, Immidiate;

    public String getPrefix() {
        switch (this) {
            case GlobalDirect:
                return "gd_";
            case GlobalIndirect:
                return "gi_";
            case LocalDirect:
                return "ld_";
            case LocalIndirect:
                return "li_";
            case Immidiate:
                return "im_";
        }
        return null;
    }

    public boolean isDirect() {
        return this.equals(LocalDirect) || this.equals(GlobalDirect);
    }

    public boolean isIndirect() {
        return this.equals(LocalIndirect) || this.equals(GlobalIndirect);
    }

    public AddressingMode getAsInDir() throws CodeGeneratorException {
        switch (this) {
            case GlobalDirect:
                return GlobalIndirect;
            case GlobalIndirect:
                return GlobalIndirect;
            case LocalDirect:
                return LocalIndirect;
            case LocalIndirect:
                return LocalIndirect;
        }
        throw new CodeGeneratorException("immidiate cant be indir");
    }

    public AddressingMode getAsDir() throws CodeGeneratorException {
        switch (this) {
            case GlobalDirect:
                return GlobalDirect;
            case GlobalIndirect:
                return GlobalDirect;
            case LocalDirect:
                return LocalDirect;
            case LocalIndirect:
                return LocalDirect;
        }
        throw new CodeGeneratorException("immidiate cant be dir");
    }
}
