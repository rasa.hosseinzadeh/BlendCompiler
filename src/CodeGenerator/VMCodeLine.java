package CodeGenerator;

import CodeGenerator.Descriptors.PrimitiveVariableType;

/**
 * Created by rasa on 2/1/17.
 */
public class VMCodeLine {
    private OpCode Operator;
    private Operand firstOperand, secondOperand, thirdOperand;


    /**
     * just pass null the variables that are not used
     * for operators that have less than 3 operands
     *
     * @param operator
     * @param first
     * @param second
     * @param third
     */
    public VMCodeLine(OpCode operator, Operand first, Operand second, Operand third) {
        Operator = operator;
        this.firstOperand = first;
        this.secondOperand = second;
        this.thirdOperand = third;
    }


    public static VMCodeLine add(Operand first, Operand second, Operand result) {
        return new VMCodeLine(OpCode.Add, first, second, result);
    }

    /**
     *
     * @param first
     * @param second
     * @param result
     * @return code for result = first - second
     */
    public static VMCodeLine subtract(Operand first, Operand second, Operand result) {
        return new VMCodeLine(OpCode.Subtract, first, second, result);
    }

    public static VMCodeLine multiply(Operand first, Operand second, Operand result) {
        return new VMCodeLine(OpCode.Multiply, first, second, result);
    }

    public static VMCodeLine divide(Operand first, Operand second, Operand result) {
        return new VMCodeLine(OpCode.Divide, first, second, result);
    }

    /**
     * @param first
     * @param second
     * @param result
     * @return code for result = first % second
     */
    public static VMCodeLine mod(Operand first, Operand second, Operand result) {
        return new VMCodeLine(OpCode.Mod, first, second, result);
    }

    public static VMCodeLine logicalAnd(Operand first, Operand second, Operand result) {
        return new VMCodeLine(OpCode.LogicalAnd, first, second, result);
    }

    public static VMCodeLine logicalOr(Operand first, Operand second, Operand result) {
        return new VMCodeLine(OpCode.LogicalOr, first, second, result);
    }

    public static VMCodeLine binaryAnd(Operand first, Operand second, Operand result) {
        return new VMCodeLine(OpCode.BinaryAnd, first, second, result);
    }

    public static VMCodeLine binaryOr(Operand first, Operand second, Operand result) {
        return new VMCodeLine(OpCode.BinaryOr, first, second, result);
    }

    public static VMCodeLine binaryXor(Operand first, Operand second, Operand result) {
        return new VMCodeLine(OpCode.BinaryXor, first, second, result);
    }

    public static VMCodeLine binaryNot(Operand operand, Operand result) {
        return new VMCodeLine(OpCode.BinaryNot, operand, result, null);
    }

    public static VMCodeLine lessThan(Operand less, Operand more, Operand result) {
        return new VMCodeLine(OpCode.LessThan, less, more, result);
    }

    public static VMCodeLine greaterThan(Operand more, Operand less, Operand result) {
        return new VMCodeLine(OpCode.GreaterThan, more, less, result);
        //also new VMCodeLine(OpCode.LessThan, less, more, result); #harhar
    }

    public static VMCodeLine lessThanOrEqual(Operand less, Operand more, Operand result) {
        return new VMCodeLine(OpCode.LessThanEqual, less, more, result);
    }

    public static VMCodeLine greaterThanOrEqual(Operand more, Operand less, Operand result) {
        return new VMCodeLine(OpCode.GreaterThanEqual, more, less, result);
    }

    public static VMCodeLine equals(Operand first, Operand second, Operand result) {
        return new VMCodeLine(OpCode.EqualityCheck, first, second, result);
    }

    public static VMCodeLine notEquals(Operand first, Operand second, Operand result) {
        return new VMCodeLine(OpCode.NotEqual, first, second, result);
    }

    public static VMCodeLine logicalNot(Operand operand, Operand result) {
        return new VMCodeLine(OpCode.LogicalNot, operand, result, null);
    }

    public static VMCodeLine unaryMinus(Operand operand, Operand result) {
        return new VMCodeLine(OpCode.UnaryMinus, operand, result, null);
    }

    public static VMCodeLine assign(Operand operand, Operand result) {
        return new VMCodeLine(OpCode.Assignment, operand, result, null);
    }

    public static VMCodeLine jumpZero(Operand condition, Operand address) {
        return new VMCodeLine(OpCode.JumpZero, condition, address, null);
    }

    public static VMCodeLine jump(Operand address) {
        return new VMCodeLine(OpCode.Jump, address, null, null);
    }

    public static void completeJump(VMCodeLine jumpCode, int jumpAdr) {
        jumpCode.firstOperand = new Operand(AddressingMode.Immidiate, PrimitiveVariableType.Int, jumpAdr);
    }

    public static void completeJumpZero(VMCodeLine jumpCode, int jumpAdr, AddressingMode mode) {
        jumpCode.secondOperand = new Operand(mode, PrimitiveVariableType.Int, jumpAdr);
    }

    public static VMCodeLine writeInteger(Operand output) {
        return new VMCodeLine(OpCode.WriteInteger, output, null, null);
    }

    public static VMCodeLine writeFloat(Operand output) {
        return new VMCodeLine(OpCode.WriteFloat, output, null, null);
    }

    public static VMCodeLine writeText(Operand output) {
        return new VMCodeLine(OpCode.WriteText, output, null, null);
    }

    public static VMCodeLine readInteger(Operand input) {
        return new VMCodeLine(OpCode.ReadInteger, input, null, null);
    }

    public static VMCodeLine readFloat(Operand input) {
        return new VMCodeLine(OpCode.ReadFloat, input, null, null);
    }


    public static VMCodeLine readText(Operand input) {
        return new VMCodeLine(OpCode.ReadText, input, null, null);
    }

    public static VMCodeLine logicalLeftShift(Operand shiftCount, Operand shifted) {
        return new VMCodeLine(OpCode.LogicalLeftShift, shiftCount, shifted, null);
    }

    public static VMCodeLine logicalRightShift(Operand shiftCount, Operand shifted) {
        return new VMCodeLine(OpCode.LogicalRightShift, shiftCount, shifted, null);
    }

    public static VMCodeLine increaseSP(Operand operand) {
        return new VMCodeLine(OpCode.IncSp, operand, null, null);
    }

    public static VMCodeLine decreaseSP(Operand operand) {
        return new VMCodeLine(OpCode.DecSp, operand, null, null);
    }
    /**
     * @param size
     * @param address is output of machine code
     * @return
     */
    public static VMCodeLine getMemory(Operand size, Operand address) {
        return new VMCodeLine(OpCode.GetMemory, size, address, null);
    }

    public static VMCodeLine freeMemory(Operand address, Operand size) {
        return new VMCodeLine(OpCode.FreeMemory, address, size, null);
    }

    public static VMCodeLine getPC(Operand result) {
        return new VMCodeLine(OpCode.PcGet, result, null, null);
    }

    public static VMCodeLine getSP(Operand result) {
        return new VMCodeLine(OpCode.SpGet, result, null, null);
    }

    public static VMCodeLine setSP(Operand address) {
        return new VMCodeLine(OpCode.AssignSp, address, null, null);
    }

    public static VMCodeLine NOP() {
        Operand op = new Operand(AddressingMode.GlobalDirect, PrimitiveVariableType.Int, 0);
        return VMCodeLine.assign(op, op);
    }

    public static VMCodeLine getOverFlow(Operand operand) {
        return new VMCodeLine(OpCode.GetOverFlow, operand, null, null);
    }
    /**
     * Primary usage is when saving code to file
     *
     * @return this line of code as string expected by VM
     */
    @Override
    public String toString() {
        String result = Operator.toString();
        if (Operator.getOperandCount() > 0) {
            result += " " + firstOperand.toString();
        }
        if (Operator.getOperandCount() > 1) {
            result += " " + secondOperand.toString();
        }
        if (Operator.getOperandCount() > 2) {
            result += " " + thirdOperand.toString();
        }
        return result;
    }

}
