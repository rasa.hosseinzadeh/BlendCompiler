package Scanner;

import Exceptions.ScannerException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.lang.reflect.Field;
import java.util.HashMap;

/**
 * This is the template of class 'scanner'. You should place your own 'scanner class here and
 * your scanner should match this interface.
 */
public class Scanner {
    private static HashMap<Integer, String> num_lexems = new HashMap();
    private int lineNumber = 1;
    private FlexScanner flex;
    private Symbol currentSymbol;

    public Scanner(String fileName) throws ScannerException {
        Reader reader = null;
        try {
            reader = new FileReader(fileName);
            flex = new FlexScanner(reader);
            for (Field x : Symbols.class.getDeclaredFields()) {
                num_lexems.put(x.getInt(null), x.getName());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public String nextToken() throws ScannerException {
        try {
            currentSymbol = flex.next_token();
        } catch (IOException e) {
            throw new ScannerException();
        }
        lineNumber = flex.getCurrentLineNumber();
        return num_lexems.get(currentSymbol.getSymbol());
    }

    public Object tokenValue() {
        if (currentSymbol.getValue() instanceof Long) {
            Long l = (Long) currentSymbol.getValue();
            if (l <= Integer.MAX_VALUE && l >= Integer.MIN_VALUE) {
                return Math.toIntExact(l);
            }
        }
        return currentSymbol.getValue();
    }

    public Boolean hasValue() {
        return !currentSymbol.isNull();
    }

    public String matchedText() {
        return flex.yytext();
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public int currentToken() {
        return currentSymbol.getSymbol();
    }
}
