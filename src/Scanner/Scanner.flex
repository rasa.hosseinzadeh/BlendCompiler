%%

%public
%class FlexScanner
%implements Symbols

%unicode

%line
%column

%function next_token
%type Symbol
%eofclose

%{
  StringBuilder string = new StringBuilder();
  boolean inAssign = false;
  public int getCurrentLineNumber() {
       return yyline;
  }

  public int getCurrentCharNumber() {
       return yychar;
  }
  public int getCurrentColumnNumber() {
       return yycolumn;
  }
  private long parseLong(int start, int end, int radix) {
    long result = 0;
    long digit;

    for (int i = start; i < end; i++) {
      digit  = Character.digit(yycharat(i),radix);
      result=Math.multiplyExact(result,radix);
      result=Math.addExact(result,digit);
    }

    return result;
  }
%}

/* main character classes */
EndOfLine = \r|\n|\r\n
InputCharacter = [^\r\n]

WhiteSpace = {EndOfLine} | [ \t\f\v]

/* comments */
Comment = {MultiLineComment} | {SingleLineComment}

MultiLineComment = "<--"~"-->"
SingleLineComment = "--" {InputCharacter}* {EndOfLine}?

/* identifiers */
Identifier = [a-zA-Z][a-zA-Z0-9_]*

/* integer literals */
DecLiteral = [0-9]+

HexLiteral = 0[xX][0-9a-fA-F]*


/* floating point literals */
FloatLiteral  = ({FLit1}|{FLit2})

FLit1    = [0-9]+ \. [0-9]*
FLit2    = \. [0-9]+


/* string and character literals */
StringCharacter = [^\r\n\"\\]
SingleCharacter = [^\r\n\'\\]

%state STRING, CHARLITERAL

%%

<YYINITIAL> {

  /*Special words becuase of avoiding implementation of function calls*/
  "concat"                       { return new Symbol(CONCAT);}
  "strlen"                       { return new Symbol(STRLEN);}
  "write"                        { return new Symbol(WRITE);}
  "read"                         { return new Symbol(READ);}
  "main"                         { return new Symbol(MAIN);}

  /* keywords */
  "assign"                       { inAssign=true;return new Symbol(ASSIGN);}
  "array"                        { return new Symbol(ARRAY);}
  "bool"                         { return new Symbol(TYPE,"bool");}
  "break"                        { return new Symbol(BREAK); }
  "case"                         { return new Symbol(CASE); }
  "char"                         { return new Symbol(TYPE,"char"); }
  "continue"                     { return new Symbol(CONTINUE); }
  "do"                           { return new Symbol(DO); }
  "else"                         { return new Symbol(ELSE); }
  "endcase"                      { return new Symbol(ENDCASE); }
  "environment"                  { return new Symbol(ENVIRONMENT); }
  "true"                         { return new Symbol(CONSTANT, true); }
  "false"                        { return new Symbol(CONSTANT, false); }
  "function"                     { return new Symbol(FUNCTION);}
  "goto"                         { return new Symbol(GOTO);}
  "if"                           { return new Symbol(IF); }
  "int"                          { return new Symbol(TYPE,"int"); }
  "isvoid"                       { return new Symbol(ISVOID); }
  "label"                        { return new Symbol(LABEL); }
  "late"                         { return new Symbol(LATE); }
  "long"                         { return new Symbol(TYPE,"long"); }
  "of"                           { return new Symbol(OF); }
  "out"                          { return new Symbol(OUT); }
  "real"                         { return new Symbol(TYPE,"real"); }
  "release"                      { return new Symbol(RELEASE);}
  "return"                       { return new Symbol(RETURN); }
  "string"                       { return new Symbol(TYPE,"string"); }
  "structure"                    { return new Symbol(STRUCTURE);}
  "void"                         { return new Symbol(VOID); }
  "while"                        { return new Symbol(WHILE); }



  /* separators */
  "("                            { return new Symbol(LPAREN); }
  ")"                            { return new Symbol(RPAREN); }
  "{"                            { return new Symbol(LBRACE); }
  "}"                            { return new Symbol(RBRACE); }
  "["                            { return new Symbol(LBRACKET); }
  "]"                            { return new Symbol(RBRACKET); }
  ";"                            { inAssign=false;return new Symbol(SEMICOLON); }
  ":"                            { return new Symbol(COLON); }
  ","                            { return new Symbol(COMMA); }
  "."                            { return new Symbol(DOT); }

  /* operators */
  ":="                           { return new Symbol(EQ); }
  "="                            { return new Symbol(EQCOMP); }
  ">"                            { if(inAssign){inAssign=false;return new Symbol(GT_IN_ASSIGN);}else{return new Symbol(GT);} }
  "<"                            { if(inAssign){return new Symbol(LT_IN_ASSIGN);}else{return new Symbol(LT);} }
  "!"                            { return new Symbol(NOT); }
  "~"                            { return new Symbol(COMP); }
  "::"                           { return new Symbol(DOUBLECOLON); }
  "<="                           { return new Symbol(LTEQ); }
  ">="                           { return new Symbol(GTEQ); }
  "!="                           { return new Symbol(NOTEQ); }
  "&&"                           { return new Symbol(LOGICALAND); }
  "||"                           { return new Symbol(LOGICALOR); }
  "+"                            { return new Symbol(PLUS); }
  "-"                            { return new Symbol(MINUS); }
  "*"                            { return new Symbol(MUL); }
  "/"                            { return new Symbol(DIV); }
  "&"                            { return new Symbol(AND); }
  "|"                            { return new Symbol(OR); }
  "^"                            { return new Symbol(XOR); }
  "%"                            { return new Symbol(MOD); }

  /* string literal */
  \"                             { yybegin(STRING); string.setLength(0); }

  /* character literal */
  \'                             { yybegin(CHARLITERAL); }

  /* numeric literals Always Long if there are int cast them in cg or parser or scanner class*/

  {DecLiteral}                   { return new Symbol(CONSTANT, new Long(yytext().substring(0,yylength()))); }
  {HexLiteral}                   { return new Symbol(CONSTANT, new Long(parseLong(2, yylength(), 16))); }


  {FloatLiteral}                 { return new Symbol(CONSTANT, new Float(yytext().substring(0,yylength()))); }

  /* comments */
  {Comment}                      { }

  /* whitespace */
  {WhiteSpace}                   { }

  /* identifiers */
  {Identifier}                   { return new Symbol(IDENTIFIER, yytext()); }
}

<STRING> {
  \"                             { yybegin(YYINITIAL); return new Symbol(CONSTANT, string.toString()); }

  {StringCharacter}+             { string.append( yytext() ); }

  /* escape sequences */
  "\\b"                          { string.append( '\b' ); }
  "\\t"                          { string.append( '\t' ); }
  "\\n"                          { string.append( '\n' ); }
  "\\f"                          { string.append( '\f' ); }
  "\\r"                          { string.append( '\r' ); }
  "\\\""                         { string.append( '\"' ); }
  "\\'"                          { string.append( '\'' ); }
  "\\\\"                         { string.append( '\\' ); }

  /* error cases */
  \\.                            { throw new RuntimeException("Illegal escape sequence \""+yytext()+"\""); }
  {EndOfLine}                    { throw new RuntimeException("Unterminated string at end of line"); }
}

<CHARLITERAL> {
  {SingleCharacter}\'            { yybegin(YYINITIAL); return new Symbol(CONSTANT, yytext().charAt(0)); }

  /* escape sequences */
  "\\b"\'                        { yybegin(YYINITIAL); return new Symbol(CONSTANT, '\b');}
  "\\t"\'                        { yybegin(YYINITIAL); return new Symbol(CONSTANT, '\t');}
  "\\n"\'                        { yybegin(YYINITIAL); return new Symbol(CONSTANT, '\n');}
  "\\f"\'                        { yybegin(YYINITIAL); return new Symbol(CONSTANT, '\f');}
  "\\r"\'                        { yybegin(YYINITIAL); return new Symbol(CONSTANT, '\r');}
  "\\\""\'                       { yybegin(YYINITIAL); return new Symbol(CONSTANT, '\"');}
  "\\'"\'                        { yybegin(YYINITIAL); return new Symbol(CONSTANT, '\'');}
  "\\\\"\'                       { yybegin(YYINITIAL); return new Symbol(CONSTANT, '\\'); }

  /* error cases */
  \\.                            { throw new RuntimeException("Illegal escape sequence \""+yytext()+"\""); }
  {EndOfLine}                    { throw new RuntimeException("Unterminated character literal at end of line"); }
}

/* error fallback */
[^]                              { throw new RuntimeException("Illegal character \""+yytext()+
                                                              "\" at line "+yyline+", column "+yycolumn); }
<<EOF>>                          { return new Symbol(EOF); }
