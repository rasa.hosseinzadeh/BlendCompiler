package Scanner;

/**
 * Created by rasa on 2/3/17.
 */
public interface ScannerTypes {
    String BOOL = "bool";
    String LONG = "long";
    String REAL = "real";
    String CHAR = "char";
    String STRINGTYPE = "string";
    String INT = "int";
}
