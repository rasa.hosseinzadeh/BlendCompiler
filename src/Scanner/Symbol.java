package Scanner;

/**
 * Created by rasa on 1/23/17.
 */
public class Symbol {
    private int symbol;
    private Object value;

    public Symbol(int symbol, Object value) {
        this.symbol = symbol;
        this.value = value;
    }

    public Symbol(int symbol) {
        this.symbol = symbol;
        this.value = null;
    }

    public int getSymbol() {
        return symbol;
    }

    public Object getValue() {
        return value;
    }

    public boolean isNull() {
        return (value == null);
    }

    public boolean isEOF() {
        return (symbol == Symbols.EOF);
    }
}
