package Parser;

import CodeGenerator.CodeGenerator;
import Exceptions.CodeGeneratorException;
import Exceptions.ParserException;
import Exceptions.ScannerException;
import Scanner.Scanner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Stack;

public class Parser {
    private final static HashMap<String, String> translator = new HashMap<>();

    static { // initTranslator
        translator.put("GTEQ", ">=");
        translator.put("FUNCTION", "function");
        translator.put("LBRACKET", "[");
        translator.put("LTEQ", "<=");
        translator.put("CONCAT", "concat");
        translator.put("XOR", "^");
        translator.put("CASE", "case");
        translator.put("LPAREN", "(");
        translator.put("CONTINUE", "continue");
        translator.put("ARRAY", "array");
        translator.put("OUT", "out");
        translator.put("MINUS", "-");
        translator.put("WRITE", "write");
        translator.put("RPAREN", ")");
        translator.put("AND", "&");
        translator.put("NOT", "!");
        translator.put("SEMICOLON", ";");
        translator.put("LOGICALOR", "||");
        translator.put("LT", "<");
        translator.put("TYPE", "TYPE");
        translator.put("OR", "|");
        translator.put("COMMA", ",");
        translator.put("STRLEN", "strlen");
        translator.put("COMP", "~");
        translator.put("DIV", "/");
        translator.put("PLUS", "+");
        translator.put("ASSIGN", "assign");
        translator.put("MAIN", "main");
        translator.put("IF", "if");
        translator.put("DOT", ".");
        translator.put("LOGICALAND", "&&");
        translator.put("OF", "of");
        translator.put("EOF", "$");
//        translator.put("$", "EOF");
        translator.put("RETURN", "return");
        translator.put("RBRACKET", "]");
        translator.put("error", "error");
        translator.put("LATE", "late");
        translator.put("MUL", "*");
        translator.put("ENVIRONMENT", "environment");
        translator.put("MOD", "%");
        translator.put("ISVOID", "isvoid");
        translator.put("BREAK", "break");
        translator.put("CONSTANT", "CONSTANT");
        translator.put("EQ", ":=");
        translator.put("COLON", ":");
        translator.put("LBRACE", "{");
        translator.put("ELSE", "else");
        translator.put("READ", "read");
        translator.put("GOTO", "goto");
        translator.put("WHILE", "while");
        translator.put("EQCOMP", "=");
        translator.put("RBRACE", "}");
        translator.put("STRUCTURE", "structure");
        translator.put("LABEL", "label");
        translator.put("GT", ">");
        translator.put("DOUBLECOLON", "::");
        translator.put("ENDCASE", "endcase");
        translator.put("DO", "do");
        translator.put("NOTEQ", "!=");
        translator.put("RELEASE", "release");
        translator.put("IDENTIFIER", "id");
        translator.put("VOID", "void");
        translator.put("LT_IN_ASSIGN", "<_in_assign");
        translator.put("GT_IN_ASSIGN", ">_in_assign");
    }

    Scanner scanner;
    CodeGenerator cg;
    PTBlock[][] parseTable;
    Stack<Integer> parseStack = new Stack<Integer>();
    String[] symbols;
    private HashMap<String, Integer> token_num;

    public Parser(String inputFile, String[] symbols, PTBlock[][] parseTable) {
        try {
            this.parseTable = parseTable;
            this.symbols = symbols;
            this.token_num = new HashMap<>();
            for (int i = 0; i < symbols.length; ++i) {
                token_num.put(symbols[i], i);
            }
            scanner = new Scanner(inputFile);
            cg = new CodeGenerator(scanner);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int LineNumber() {
        return scanner.getLineNumber(); // Or any other name you used in your Scanner
    }

    public void Parse(boolean generateCode) throws ParserException, ScannerException, CodeGeneratorException {
        try {
            cg.setCodeGeneration(generateCode);
            int tokenId = nextTokenID();
            int curNode = 0;
            boolean notAccepted = true;
            while (notAccepted) {
                //System.err.println(curNode);
                String token = symbols[tokenId];
                PTBlock ptb = parseTable[curNode][tokenId];
                switch (ptb.getAct()) {
                    case PTBlock.ActionType.Error: {
                        throw new ParserException(String.format("Compile Error (" + token + ") at line " + scanner.getLineNumber() + " @ " + curNode));
                    }
                    case PTBlock.ActionType.Shift: {
                        cg.Generate(ptb.getSem());
                        tokenId = nextTokenID();
                        curNode = ptb.getIndex();
                    }
                    break;

                    case PTBlock.ActionType.PushGoto: {
                        parseStack.push(curNode);
                        curNode = ptb.getIndex();
                    }
                    break;

                    case PTBlock.ActionType.Reduce: {
                        if (parseStack.size() == 0) {
                            throw new ParserException(String.format("Compile Error (" + token + ") at line " + scanner.getLineNumber() + " @ " + curNode));
                        }

                        curNode = parseStack.pop();
                        ptb = parseTable[curNode][ptb.getIndex()];
                        cg.Generate(ptb.getSem());
                        curNode = ptb.getIndex();
                    }
                    break;

                    case PTBlock.ActionType.Accept: {
                        notAccepted = false;
                    }
                    break;

                }
            }
            cg.FinishCode();
        } catch (ArithmeticException | NumberFormatException e) {
            throw new ParserException(String.format("Compile Error: LongInt number out of range(" + scanner.matchedText() + ") at line " + scanner.getLineNumber()));
        }
    }

    private int nextTokenID() throws ParserException, ScannerException {

        String t = translator.get(scanner.nextToken());
        System.err.println("Token: " + t);
        System.err.println("Matched: " + scanner.matchedText());

        if (scanner.hasValue()) System.err.println("Value: " + scanner.tokenValue());
        if ("CONSTANT".equals(t) && scanner.tokenValue() instanceof Float && Float.isInfinite((Float) scanner.tokenValue())) {
            throw new ParserException(String.format("Compile Error: Real number out of range(" + scanner.matchedText() + ") at line " + scanner.getLineNumber()));
        }
        Integer i = token_num.get(t);


        if (i == null) {
            throw new ScannerException("Undefined token: " + scanner.matchedText());
        }

        return i;
    }

    public void WriteOutput(String outputFile) throws IOException // You can change this function, if you think it is not comfortable
    {
        cg.WriteOutput(outputFile);
    }
}


