package Exceptions;

/**
 * Created by rasa on 2/2/17.
 */
public class CodeGeneratorException extends Exception {
    public CodeGeneratorException() {
    }

    public CodeGeneratorException(String message) {
        super(message);
    }
}
