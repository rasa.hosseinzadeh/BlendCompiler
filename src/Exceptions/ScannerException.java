package Exceptions;

/**
 * Created by rasa on 2/2/17.
 */
public class ScannerException extends Exception {
    public ScannerException(String message) {
        super(message);
    }

    public ScannerException() {
    }
}
