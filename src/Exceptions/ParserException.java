package Exceptions;

/**
 * Created by rasa on 2/2/17.
 */
public class ParserException extends Exception {
    public ParserException(String message) {
        super(message);
    }

    public ParserException() {
    }
}
